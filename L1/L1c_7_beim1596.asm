;Balázs Éva-Tünde, beim1596, 511/1
;Labor 1, C
;7.
;ha d mod 4 = 0 : (b - a) * 2
;ha d mod 4 = 1 : 13 - c
;ha d mod 4 = 2 : (9 - b) div a
;ha d mod 4 = 3 : c * (a - c)

%include 'io.inc'

global main 

section .text
main:
	mov 	eax, feladat
	call 	io_writestr
	call	io_writeln

	mov 	eax, szovega
	call 	io_writestr
    call    io_readint
    mov 	[a], eax

	
	mov 	eax, szovegb
	call 	io_writestr
    call    io_readint
    mov 	[b], eax

	
	mov 	eax, szovegc
	call 	io_writestr
    call    io_readint
    mov 	[c], eax
	
	mov 	eax, szovegd
	call 	io_writestr
    call    io_readint
    mov 	[d], eax
	
	mov		ebx,4
	cdq
	idiv	ebx
	cmp		edx,3
	je		harom
	cmp		edx,2
	je		ketto
	cmp		edx,1
	je		egy
	cmp		edx,0
	je		nulla
	cmp		edx,-1
	je		harom
	cmp		edx,-2
	je		ketto
	cmp		edx,-3
	je		egy
	
	harom:
	; c * (a - c)
	
	mov 	eax,str_3
	call	io_writestr
	mov 	eax,[a]
	sub		eax,[c]
	imul	dword[c]
	call	io_writeint
	ret
	ketto:
	; (9 - b) div a
	
	mov 	eax,str_2
	call	io_writestr
	mov 	eax,9
	sub		eax,[b]
	cdq
	idiv	dword[a]
	call	io_writeint
	ret
	egy:
	;13 - c
	
	mov 	eax,str_1
	call	io_writestr
	mov		eax,13
	sub		eax,[c]
	call	io_writeint
	ret
	nulla:
	; (b - a) * 2
	
	mov 	eax,str_0
	call	io_writestr
	mov 	eax,[b]
	sub		eax,[a]
	mov		ebx,2
	imul	ebx
	call	io_writeint
    ret
section .data
	a dd 0
	b dd 0
	c dd 0
	d dd 0
	szovega db 'a=',0
	szovegb db 'b=',0
	szovegc db 'c=',0
	szovegd db 'd=',0
	str_3	db 'c * (a - c)=',0
	str_2	db '(9 - b) div a=',0
	str_1	db '13 - c=',0
	str_0	db '(b - a) * 2=',0
	feladat db 'ha d mod 4 = 0 : (b - a) * 2 ha d mod 4 = 1 : 13 - c ha d mod 4 = 2 : (9 - b) div a ha d mod 4 = 3 : c * (a - c)',0
	