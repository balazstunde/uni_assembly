;Balázs Éva-Tünde, beim1596, 511/1
;Labor1, A
;aritmetikai kifejezés kiértékelése, „div” az egész osztás hányadosát, „mod” pedig a maradékát jelenti. 
;a, b, c, d, e, f, g előjeles egész számok, az io_readint függvénnyel olvassuk be őket. Az eredményt az io_writeint eljárás segítségével írjuk ki.
;7. ((a - (b * c)) div (a - b)) + ((b - e) div c) - ((f + g - 10) mod (g div 5)) - d
;E(34,40,49,24,24,18,7) = 297
%include 'io.inc'

global main 

section .text
main:
	mov 	eax,feladat
	call 	io_writestr
    call    io_readint
	
	mov 	eax, szovega
	call 	io_writestr
    call    io_readint
    mov 	[a], eax

	
	mov 	eax, szovegb
	call 	io_writestr
    call    io_readint
    mov 	[b], eax

	
	mov 	eax, szovegc
	call 	io_writestr
    call    io_readint
    mov 	[c], eax
	
	;((a-(b*c)) div (a-b))
	mov 	eax,[b]
	imul 	eax,[c]
	mov		edx,[a]
	sub		edx,eax	;edx-ben az elso zarojel
	xchg	eax,edx ;eax-ben az elso zarojel
	mov 	ecx,[a]
	sub		ecx,[b];ecx-ben a masodik zarojel
	xor		edx,edx
	cdq
	idiv		ecx;
	mov 	ecx,eax	;ecx-ben van az elso nagy zarojel
	

	
	mov 	eax, szovegd
	call 	io_writestr
    call    io_readint
    mov 	[d], eax

	
	mov 	eax, szovege
	call 	io_writestr
    call    io_readint
    mov 	[e], eax
	
	;(b-e)div c
	mov		ebx,[b]
	sub		ebx,[e]
	mov 	eax,ebx
	;xor		edx, edx
	cdq
	idiv	dword [c]
	add		ecx,eax ;ismet ecx-ben van az elso ket nagy zarojel  ((a - (b * c)) div (a - b)) + ((b - e) div c) 
	
	
	mov 	eax, szovegf
	call 	io_writestr
    call    io_readint
    mov 	[f], eax
	
	mov 	eax, szovegg
	call 	io_writestr
    call    io_readint
    mov 	[g], eax
	
	;(f + g - 10)
	mov 	ebx,[f]
	add		ebx,[g]
	sub		ebx,10;ebx-ben van ez a zarojel
	
	;g div 5
	mov 	eax,[g]
	push	ebx
	mov		ebx,5
	cdq
	idiv	 ebx;most eax-ban van az utolso zarojel
	pop		ebx
	
	;(f + g - 10)mod(g div 5)
	xchg	eax,ebx
	cdq
	idiv	ebx;edx-ben van a masodik nagy zarojel
	
	;((a - (b * c)) div (a - b)) + ((b - e) div c)-(f + g - 10)mod(g div 5)-d (ecx-edx)-d
	sub		ecx,edx
	sub		ecx,[d]
	mov		eax,kiiras
	call 	io_writestr
	mov 	eax,ecx
	call 	io_writeint

    ret
section .data
	a dd 0
	b dd 0
	c dd 0
	d dd 0
	e dd 0
	f dd 0
	g dd 0
	szovega db 'a=',0
	szovegb db 'b=',0
	szovegc db 'c=',0
	szovegd db 'd=',0
	szovege db 'e=',0
	szovegf db 'f=',0
	szovegg db 'g=',0
	kiiras	db '((a - (b * c)) div (a - b)) + ((b - e) div c) - ((f + g - 10) mod (g div 5)) - d=',0
	feladat	db	'E(a,b,c,d,e,f,g)= ((a - (b * c)) div (a - b)) + ((b - e) div c) - ((f + g - 10) mod (g div 5)) - d',0