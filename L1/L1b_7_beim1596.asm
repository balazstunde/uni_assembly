;Balázs Éva-Tünde, beim1596, 511/1
;Labor1,B
;Írassuk ki a kiértékelendő kifejezést, olvassuk be az a, b, c, d értékeket az io_readint függvény segítségével, majd írassuk ki a beolvasott értékeket és az eredményt egymás alá bináris formában az io_writebin függvény segítségével.
;7. a OR ((b XOR c) AND ((NOT a) XOR d))
%include 'io.inc'

global main 

section .text
main:
	mov 	eax,kiir
	call	io_writestr
	call	io_writeln

	mov 	eax,szovega
	call	io_writestr
	call	io_readint
	mov 	[a],eax;
	
	mov 	eax,szovegb
	call	io_writestr
	call	io_readint
	mov 	[b],eax;
	
	mov 	eax,szovegc
	call	io_writestr
	call	io_readint
	mov 	[c],eax;
	
	mov 	eax,szovegd
	call	io_writestr
	call	io_readint
	mov 	[d],eax;
	
	; a OR ((b XOR c) AND ((NOT a) XOR d))
	mov 	ebx,[b]
	xor		ebx,[c]
	mov		eax,ebx
	mov		eax,[a]
	not		eax
	xor		eax,[d]
	and		ebx,eax
	mov 	eax,[a]
	or		eax,ebx
	mov 	ebx,eax
	
	;kiiras
	mov		eax,[a]
	call	io_writebin
	call	io_writeln
	
	mov		eax,[b]
	call	io_writebin
	call	io_writeln
	
	mov		eax,[c]
	call	io_writebin
	call	io_writeln
	
	mov		eax,[d]
	call	io_writebin
	call	io_writeln
	
	mov 	eax,eredmeny
	call	io_writestr
	call	io_writeln
	
	mov		eax,ebx
	call	io_writebin
	
    ret
section .data
	a dd 0
	b dd 0
	c dd 0
	d dd 0
	szovega db 'a=',0
	szovegb db 'b=',0
	szovegc db 'c=',0
	szovegd db 'd=',0
	kiir	db 'a OR ((b XOR c) AND ((NOT a) XOR d))',0
	eredmeny db 'Eredmeny=a OR ((b XOR c) AND ((NOT a) XOR d))=',0
