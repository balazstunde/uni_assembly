;Balázs Éva-Tünde, beim1596, 511/1
;Készítsünk főprogramot, amely ezt a négy függvényt alkalmazza:

;beolvas egy számot decimális egészként,
;kiíratja decimális (előjeles egész) alakban,
;kiíratja hexadecimális (a szám komplementerkódbeli ábrázolása) alakban,
;beolvas egy másik számot hexadecimális egészként (ezt egy egész komplementerkódbeli ábrázolásának tekintjük),
;kiíratja decimális (előjeles egész) alakban,
;kiíratja hexadecimális alakban,
;a két számot összeadja,
;az eredményt kiíratja decimális (előjeles egész) alakban,
;az eredményt kiíratja hexadecimális alakban.
%include 'mio.inc'
;%include 'io.inc'
global main 

section .text
;--------------------------------------------------------------***-----------------------------------------------------------------------------------
	;kiiras decimalis
kiir:
		push ebx
		push ecx
		push eax
		push edx
	xor		edx,edx
	xor		ebx,ebx
	xor		ecx,ecx
	;mov		eax,-1
	mov		edx,eax
	mov		eax,decikiir
	call	mio_writestr
	mov		eax,edx
	mov 	ebx, 10		;10-zel kell osztanom majd 
	cmp		eax, 0		;ha az eax nem kisebb mint nulla egybol ismetlesbe lepek
	jge		.loop
	neg		eax			;ha viszont negativ, akkor azt pozitivva kell alakitsam
	mov		ecx,eax
	mov		eax,'-'		;a pozitiv szam ele egy minusz jel
	call	mio_writechar
	mov		eax,ecx
	xor		ecx,ecx
	
.loop:
	xor		edx,edx			;elojel nelkul dolgozok
	div		ebx
	push	edx
	inc		ecx
	test	eax,eax		;ha nulla akkor vege lesz az ismetlesnek
	jnz		.loop
	
.loop2:
	pop 	eax
	add		eax,'0'		;mivel kivontam hozza is kell adjam, hogy karakter legyen
	call	mio_writechar
	dec		ecx
	cmp		ecx, 0
	je		.vege
	jmp		.loop2
.vege:
	pop 	edx
	pop		eax
	pop		ecx
	pop		ebx
	ret
;--------------------------------------------------------------***-----------------------------------------------------------------------------------
	;beolvasas decimalis
beolvas:
		push ebx
		push ecx
		push edx
.eleje:
	mov 	eax,uzenet
	call	mio_writestr
	xor 	eax,eax			;nullazzuk az eax valtozot
	xor		ebx,ebx			;nullazzuk az ebx valtozot
	xor		ecx,ecx			;nullazzuk az ecx valtozot
	xor		edx,edx
	mov		edx,1			;edx-be egyet teszunk

	call	mio_readchar
	call	mio_writechar
	cmp		eax,'-'		;elso karakternek beolvashatunk "-"-t is
	jnz		.pozitiv	;ha nem minusz akkor pozitiv,hanem minusz egyet teszunk le a verembe
	push	-1
	inc		ecx
	jmp		.verembetesz
.pozitiv:
	cmp		eax,'0'		;az elso szamjegyet ha nem volt minusz betesszuk a verembe
	jl		.hiba
	cmp		eax,'9'
	jg		.hiba
	sub		eax,'0'
	push	eax
	inc 	ecx
.verembetesz:
	call	mio_readchar	;amig nem enter olvassuk be a karaktereket verembe, plusz hibakezeles
	call	mio_writechar
	cmp		eax, 13
	je		.verembolkivesz
	cmp		eax,'0'
	jl		.hiba
	cmp		eax,'9'
	jg		.hiba
	sub		eax,'0'
	push	eax
	inc		ecx
	jmp		.verembetesz
.verembolkivesz:		;enterre kezdjuk kiszedni a karaktereket
	pop		eax
	cmp		eax,-1		;ha -1-gyel talakozunk negalunk,ez csak utolsokent lehet
	je		.negalas
	imul	eax,edx
	imul	edx,10
	sub		ecx,1
	cmp		ecx,0
	je		.utolso
	add		ebx,eax
	jmp		.verembolkivesz

.negalas:
	neg		ebx
	jmp		.vege
.utolso:
	add		ebx,eax		;az utolso elemet is hozza kell adjam az ebx-ben felepitett szamhoz
	jmp		.vege

.hiba:
	call	mio_writeln		;hibauzenet, ujra keri a szamot
	mov		eax,hiba
	call	mio_writestr
	call	mio_writeln
	cmp		ecx,0
	jg		.kipoppol
	jmp		.eleje
	
.kipoppol:
	pop		eax		;a megmaradt verembeli ertekeket kiszedi
	dec		ecx
	cmp		ecx,0
	jne		.kipoppol
	jmp		.eleje
.vege:
	call	mio_writeln
	mov		eax,ebx
	pop 	edx
	pop		ecx
	pop		ebx
	ret
;--------------------------------------------------------------***-----------------------------------------------------------------------------------
hexakiir:
	push	eax
	push	ebx
	push	edx
	push	ecx
	
	xor		ecx,ecx
	xor		edx,edx
	xor		ebx,ebx
	mov		ecx,8
	mov		ebx,0xF0000000		;ezzel hasonlitom hogy sorrendbe tudjam kiirni
	
	mov		edx,eax
	call	mio_writeln
	mov		eax,hexabakiir
	call	mio_writestr
	mov		eax,hexaelojel
	call	mio_writestr
	mov		eax,edx
.kezd:
	cmp		ecx,0			;mikor lejart a 8 karakter kilepik
	je		.vege
	mov		edx,eax
	and		eax,ebx			;itt a hasonlitas mivel csak a felso negy bit marad meg
	shr		eax,28			;ennyit kell toljam hogy ki tudjam iratni
	cmp		eax,9			;hogyha betu atugrok
	jg		.betuk
	add		eax,'0'
	call	mio_writechar
	dec		ecx
	mov		eax,edx
	shl		eax,4			;mindig a felso 4 bitet kapom meg igy elore kell tolni
	jmp		.kezd
	
.betuk:
	add		eax,55			;nagybetukkel iratom ki
	dec		ecx
	call	mio_writechar
	mov		eax,edx
	shl		eax,4			;ugyanugy kell tolni mint az elobb
	jmp		.kezd
	
.vege:
	pop	ecx
	pop	edx
	pop	ebx
	pop	eax
	ret


;hexa szam beolvasasa
;--------------------------------------------------------------***-----------------------------------------------------------------------------------
hexabeolvas:
	
	push 	ebx
	push	edx
	push	ecx
.eleje:
	xor		ebx,ebx
	xor		edx,edx
	xor		ecx,ecx
	xor		eax,eax
	mov		eax,hexa
	mov		ecx,8		;max 8 karaktert olvashatok be
	call	mio_writeln
	call	mio_writestr
	
.kezd:
	xor		eax,eax			;biztonsagos ha mindig nulla
	call	mio_readchar
	call	mio_writechar
	cmp		eax,13			;enterre kilepik
	je		.vege
	cmp		eax,97			;ha a-nal nagyobb kicsibetu
	jge		.kicsibetu
	cmp		eax,65			;ha A-nal nagyobb nagybetu
	jge		.nagybetu
	cmp		eax,'0'
	jl		.hiba
	cmp		eax,'9'			;hibakezeles
	jg		.hiba
	shl		ebx,4			;eltolasokkal epitem fel
	sub		eax,'0'
	add		bl,al
	dec		ecx
	cmp		ecx,0
	je		.vege
	jmp		.kezd
	
.nagybetu:
	cmp		eax,70		;hibakezeles
	jg		.hiba
	shl		ebx,4
	sub		eax,55		;mindenkepp szamma kell alakitsam
	add		ebx,eax
	dec		ecx
	cmp		ecx,0
	je		.vege
	jmp		.kezd
	
.kicsibetu:
	cmp		eax,102		;hibakezeles
	jg		.hiba
	shl		ebx,4
	sub		eax,87
	add		ebx,eax
	dec		ecx
	cmp		ecx,0
	je		.vege
	jmp		.kezd

.hiba:
	call	mio_writeln
	mov		eax,hiba
	call	mio_writestr
	jmp		.eleje		;visszaugrik az elejere
	
.vege:
	mov		eax,ebx
	pop 	ecx
	pop		edx
	pop		ebx
	ret
	
main:
	call	beolvas	;decimalis szamot olvas be
	mov		ebx,eax	;megorzi az ebx-ben az eax-t
	call	kiir	;decimalist kiir
	call	hexakiir	;hexaba kiirja a decimalist
	call	hexabeolvas	;egy hexa szamot beolvas
	call 	mio_writeln
	call	kiir	;decimalisba kiir
	call	mio_writeln
	call	hexakiir	;hexaba kiir
	add		eax,ebx		;osszead ket szamot
	mov		ebx,eax
	mov		eax,osszeg
	call	mio_writeln
	call	mio_writestr
	mov		eax,ebx
	call	kiir
	mov		ebx,eax
	mov		eax,osszeg
	call	mio_writeln
	call	mio_writestr
	mov		eax,ebx
	call	hexakiir
	ret
section .data
	uzenet 		db "egy decimalis szamot=",0
	hiba		db	"Hiba: A beolvasott szam nem decimalis alaku!",0
	hexa		db	"Egy hexa szamot kernek szepen:",0
	hexaelojel	db "0x",0 
	hexabakiir 	db "hexaban=",0
	decikiir	db "decimalisan=",0
	osszeg	db "Az osszeg ",0