;Balázs Éva-Tünde, beim1596, 511/1
;Labor 6 SSE2
;SSE vektorizálás
;Olvassunk be két (A és B) azonos hosszúságú lebegőpontos értékekből álló tömböt (max. 256 elem). 
;Olvassuk be a tömbök hosszát (n integer, 4<=n<=256), majd a tömbök elemeit, minden elemet új sorból (azaz 2n lebegőpontos értéket olvasunk be).
;A tömbök minden elemére páronként végezzük el az alább megadott műveleteket, tároljuk el az eredményeket egy másik tömbben, 
;végül pedig írjuk ki azt. A feladat lényege, hogy ne skaláris utasításokat használjuk, hanem vektorosokat 
;(azaz pl. ADDSS helyett ADDPS-t használjunk 4 db. számpár párhuzamos összeadására). 
;Így a teljesítmény akár 4x nagyobb lehet. Csak 4-el osztható hosszúságú tömböket kell elfogadjon a program.

;7. E(a,b) = (b^3 - a) / (a + b) - (a - b^2 + 1) / 4
%include 'io.inc'

global main 
section	.bss
	A resd 256	
	B resd 256
	C resb 256
section .text
;-------------------------------------------------------------------*******----------------------------------------------------------------------------------
Readalong:
	push	ebx
	push	ecx
	push	edx
	push	edi
	push	esi
	mov		ebx,4		;neggyel oszthato kell legyen
.loop:
	mov		eax,szam1
	call	io_writestr
	xor		eax,eax
	xor		edx,edx
	call	io_readint
	push	eax
	div		ebx
	pop		eax
	test	edx,edx		;ha nem nulla a maradek akkor megegyszer be kell olvasni
	jnz		.loop
	
	pop		esi
	pop		edi	
	pop		edx
	pop		ecx
	pop		ebx
	ret
;-------------------------------------------------------------------*******----------------------------------------------------------------------------------
Array:
	push	eax
	push	ebx
	push	ecx
	push	edx
	push	edi
	push	esi
	mov		edx,eax		;az edx tartalmazza hogy hany elemu a tomb
	xor		ecx,ecx
.loop:
	cmp		ecx,edx
	je		.end
	call	io_readflt
	movss	[edi+ecx*4],xmm0
	inc 	ecx
	jmp		.loop
	
.end:
	pop		esi
	pop		edi	
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
;-------------------------------------------------------------------*******----------------------------------------------------------------------------------
Elvegez:
	;7. E(a,b) = (b^3 - a) / (a + b) - (a - b^2 + 1) / 4
	push	eax
	push	ebx
	push	ecx
	push	edx
	push	edi
	push	esi
	mov		edx,eax
	;movaps		xmm0,[A]
	xor			ecx,ecx
	;mov			edi,C
.loop:
	cmp			ecx,edx
	jge			.end
	movaps		xmm1,[A+ecx*4]
	movaps		xmm2,[B+ecx*4]
	;(b^3 - a)
	movaps		xmm3,xmm1
	movaps		xmm4,xmm2
	movaps		xmm1,xmm2
	mulps		xmm1,xmm2
	mulps		xmm1,xmm2		;xmm1=b^3
	movaps		xmm2,xmm3
	subps		xmm1,xmm2		;(b^3 - a)
	;(a + b)
	movaps		xmm7,xmm1		;(b^3 - a)
	movaps		xmm1,xmm3
	movaps		xmm2,xmm4
	addps		xmm1,xmm2
	movaps		xmm6,xmm1		;(a + b)
	;(b^3 - a) / (a + b)
	movaps		xmm1,xmm7
	movaps		xmm2,xmm6
	divps		xmm1,xmm2
	movaps		xmm7,xmm1		;xmm7=(b^3 - a) / (a + b)
	;(a - b^2 + 1) / 4
	movaps		xmm1,xmm4
	movaps		xmm2,xmm4
	mulps		xmm1,xmm2		;b^2
	movaps		xmm2,xmm1
	movaps		xmm1,xmm3
	subps		xmm1,xmm2		;a - b^2
	addps		xmm1,[k1_0]
	divps		xmm1,[k4_0]
	movaps		xmm2,xmm1
	;(b^3 - a) / (a + b) - (a - b^2 + 1) / 4
	movaps		xmm1,xmm7
	subps		xmm1,xmm2
	movaps		[C+ecx*4],xmm1
	add			ecx,4
	jmp			.loop
.end:
	pop		esi
	pop		edi	
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
;-------------------------------------------------------------------*******----------------------------------------------------------------------------------
Kiir:
	push	eax
	push	ebx
	push	ecx
	push	edx
	push	edi
	push	esi
	;mov		eax,4
	mov		edx,eax
	;call	io_writeint
	xor		ecx,ecx
.loop:
	cmp		ecx,edx
	je		.end
	movss	xmm0,[C+ecx*4]
	call	io_writeflt
	call	io_writeln
	inc		ecx
	jmp		.loop
.end:
	pop		esi
	pop		edi	
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
;-------------------------------------------------------------------*******----------------------------------------------------------------------------------
main:
	call		Readalong
	
	push		eax
	mov			eax,tomb1
	call		io_writestr
	pop			eax
	mov			edi,A
	call		Array
	
	push		eax
	mov			eax,tomb2
	call		io_writestr
	pop			eax
	mov			edi,B
	call		Array
	
	call		Elvegez
	call		Kiir
	ret
section .data
	k1_0		dd		1.0, 1.0, 1.0, 1.0
	k4_0		dd		4.0, 4.0, 4.0, 4.0
	szam1		db		"Egy neggyel oszthato tombhosszot! ",0
	tomb1		db		"A tomb elemei: ",0
	tomb2		db		"B tomb elemi: ",0