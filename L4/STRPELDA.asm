;megfelelő üzenet kiíratása után beolvasunk egy stringet;
;kiírjuk a hosszát;
;kiírjuk a tömörített formáját;
;kiírjuk a tömörített formáját kisbetűkre alakítva;
;megfelelő üzenet kiíratása után beolvasunk egy második stringet;
;kiírjuk a hosszát;
;kiírjuk a tömörített formáját;
;kiírjuk a tömörített formáját nagybetűkre alakítva;
;létrehozunk a memóriában egy új stringet: az első string nagybetűs verziójához hozzáfűzzük a második string kisbetűs verzióját;
;kiírjuk a létrehozott stringet;
;kiírjuk a létrehozott string hosszát;
;befejezzük a programot.

%include 'IOSTR.inc'
%include 'STRINGS.inc'
%include 'mio.inc'

global main
section .bss

szoveg2 resb 256
szoveg1	resb 256

section .text

WriteInt:
		push ebx
		push ecx
		push eax
		push edx
	xor		edx,edx
	xor		ebx,ebx
	xor		ecx,ecx
	;mov		eax,-1
	mov 	ebx, 10		;10-zel kell osztanom majd 
	cmp		eax, 0		;ha az eax nem kisebb mint nulla egybol ismetlesbe lepek
	jge		.loop
	neg		eax			;ha viszont negativ, akkor azt pozitivva kell alakitsam
	mov		ecx,eax
	mov		eax,'-'		;a pozitiv szam ele egy minusz jel
	call	mio_writechar
	mov		eax,ecx
	xor		ecx,ecx
	
.loop:
	xor		edx,edx			;elojel nelkul dolgozok
	div		ebx
	push	edx
	inc		ecx
	test	eax,eax		;ha nulla akkor vege lesz az ismetlesnek
	jnz		.loop
	
.loop2:
	pop 	eax
	add		eax,'0'		;mivel kivontam hozza is kell adjam, hogy karakter legyen
	call	mio_writechar
	dec		ecx
	cmp		ecx, 0
	je		.vege
	jmp		.loop2
.vege:
	pop 	edx
	pop		eax
	pop		ecx
	pop		ebx
	ret
main:
	;elso string
	mov		eax,inmessage1
	call	mio_writestr
	mov		edi,szoveg1
	call	ReadStr
	
	;hossza
	mov		eax,Ln
	call	mio_writestr
	mov		esi,szoveg1
	call	StrLen
	call	WriteInt
	
	;tomoritett
	call	NewLine
	mov		eax,Compakt
	call	mio_writestr
	mov		esi,szoveg1
	call	StrCompact
	mov		esi,edi
	call	WriteStr
	
	;kisbetu
	call	NewLine
	mov		eax,Lower
	call	mio_writestr
	mov		esi,edi
	call	StrLower
	call	WriteStr
	call	NewLine
	;masodik string
	mov		eax,inmessage1
	call	mio_writestr
	mov		edi,szoveg2
	call	ReadStr
	
	;hossza
	mov		eax,Ln
	call	mio_writestr
	mov		esi,szoveg2
	call	StrLen
	call	WriteInt
	
	;tomoritett
	call	NewLine
	mov		eax,Compakt
	call	mio_writestr
	mov		esi,szoveg2
	call	StrCompact
	mov		esi,edi
	call	WriteStr
	
	;nagybetu
	call	NewLine
	mov		eax,Upper
	call	mio_writestr
	mov		esi,edi
	call	StrUpper
	call	WriteStr
	
	;osszefuz
	;--->kisbetu
	call	NewLine
	mov		eax,Lower
	call	mio_writestr
	mov		esi,szoveg2
	call	StrLower
	call	WriteStr
	call	NewLine
	
	;nagybetu
	;--->nagybetu
	call	NewLine
	mov		eax,Upper
	call	mio_writestr
	mov		esi,szoveg1
	call	StrUpper
	call	WriteStr
	
	call	NewLine
	mov		eax,Cat
	call	mio_writestr
	mov		edi,szoveg1
	mov		esi,szoveg2
	call	StrCat
	mov		esi,szoveg1
	call	WriteStr
	
	;hossza
	call	NewLine
	mov		eax,Ln
	call	mio_writestr
	mov		esi,szoveg1
	call	StrLen
	call	WriteInt
	
	ret
section .data
	inmessage1	db  "Egy stringet kerek beolvasni: ",0
	Upper	db 	"Nagybetukre alakitva: ",0
	Lower	db	"Kisbetukre alakitva: ",0
	Ln	db	"Hossza: ",0
	Cat	db	"Osszefuzve: ",0
	Compakt	db	"Kompakt forma: ",0