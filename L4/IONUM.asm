;Balázs Éva-Tünde, beim1596, 511/1
;Labor4, C
;ReadInt():(EAX)                  – 32 bites előjeles egész beolvasása
;WriteInt(EAX):()                  – 32 bites előjeles egész kiírása
;ReadInt64():(EDX:EAX)      – 64 bites előjeles egész beolvasása
;WriteInt64(EDX:EAX):()      – 64 bites előjeles egész kiírása
;ReadBin():(EAX)                 – 32 bites bináris pozitív egész beolvasása
;WriteBin(EAX):()                 –                    - || -                   kiírása
;ReadBin64():(EDX:EAX)     – 64 bites bináris pozitív egész beolvasása
;WriteBin64(EDX:EAX):()     –                    - || -                   kiírása
;ReadHex():(EAX)                – 32 bites pozitív hexa beolvasása
;WriteHex(EAX):()                –                    - || -                   kiírása
;ReadHex64():(EDX:EAX)     – 64 bites pozitív hexa beolvasása
;WriteHex64(EDX:EAX):()     –                    - || -                   kiírása
%include 'mio.inc'
global ReadInt,ReadBin,ReadHex,WriteBin,WriteHex,WriteInt,WriteBin64,ReadBin64,ReadHex64,WriteHex64,WriteInt64,ReadInt64
section .text
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;ReadInt

ReadInt:
	push	ebx
	push	edx
	push	ecx
	push	edi
	push	esi
	xor		eax,eax
	;mov		edi,szam
	mov		edx,edi		;megjegyzem az elejet, hogy ne torolhessen a backspace tobbet
	xor 	ecx,ecx		;szuksegem lesz ra jobb ha 0
	xor		ebx,ebx		;szuksegem lesz ra jobb ha 0
	
.ciklus:
	call	mio_readchar
	cmp		al,13
	je		.vege1
	cmp		al,8
	jne		.ugras			;ha nem backspace, nem kell kezelni, hanem kezelem
	cmp		edi,edx
	je		.ciklus
	call	mio_writechar
	mov		al,' '
	call	mio_writechar
	mov		al,8
	call	mio_writechar
	dec		edi
	cmp		edi,ebx			
	je		.ClCarry		;hogyha az edi az elso hiba helyen van (ebx) akkor torolhetem a hibat
	jmp		.ciklus
	
.ugras:
	call	mio_writechar
	stosb
	cmp		al,'-'		;ha - akkor lehet negativ
	je		.neg		;ha elso helyen van negativ lesz hanem hibanak szamit
	sub		al,'0'
	cmp		al,0
	jb		.SetCarry	;ha nem szamjegy be kell allitani a carryt (most ecx)
	cmp		al,9
	ja		.SetCarry
	jmp		.ciklus
.SetCarry:
	cmp		ecx,1		
	jne		.First		;ha meg nem volt hiba akkor megjegyzem az elso helyet es beallitom az ecx-et
	mov		ecx,1
	jmp		.ciklus
.ClCarry:
	mov		ecx,0		;hibatorles
	jmp		.ciklus
.First:
	mov		ebx,edi		;mivel meg nincs hiba, megjegyzem a helyet
	dec		ebx			;az edi mar tovabb lepett, igy csokkenteni kell
	mov		ecx,1
	jmp		.ciklus
.SetCarryandedx:
	mov		ecx,1
	dec		edx
	jmp		.ciklus
.neg:
	inc		edx
	cmp		edi,edx
	jne		.SetCarryandedx		;ha nem az elso helyen van akkor hiba (ecx=1), de az edx-et is vissza kell allitani
	dec		edx					
	;mov		ecx,-1				;ha az elso helyen van akkor megjegyzem hogy negativ szamom van (most pont el is lehet hagyni mert lennebb ellenorzom)
	jmp		.ciklus
	
.vege1:
	call	mio_writeln
	xor		al,al			;lezar
	stosb
	cmp		ecx,1			;ha van hiba akkor nem is kezdi a szamot felepiteni
	je		.theend
	;mov		esi,szam
	mov		esi,edx
	xor		ebx,ebx
	xor		edx,edx
.szamfelepit:
	xor 	eax,eax
	lodsb
	cmp		al,0
	je		.vege2		;ha vege a stringnek vege a szamnak is
	cmp		al,'-'		;ha van akkor biztosan az elso helyen van	
	je		.negativ
	sub		al,'0'		;szamma 'alakitom' a karaktert
	cmp		ecx,0
	je		.tulcsordulP
	cmp		ecx,-1
	je		.tulcsordulN
.tulcsordulP:
	mov		edx,2147483647
	imul	ebx,10
	sub		edx,ebx
	cmp		eax,edx
	je		.JContinue
	cmp		eax,edx
	jg		.SetCarry1
	add		ebx,eax
	jmp		.szamfelepit
.tulcsordulN:
	mov		edx,2147483647
	imul	ebx,10
	sub		edx,ebx
	sub		eax,1
	cmp		eax,edx
	je		.JContinueadd
	cmp		eax,edx
	jg		.SetCarry1
	add		eax,1
	add		ebx,eax
	jmp		.szamfelepit
.JContinueadd:
	inc		eax
.JContinue:
	add		ebx,eax
	xor 	eax,eax
	lodsb
	cmp		al,0
	je		.vege2		;ha vege a stringnek vege a szamnak is, nem csordul tul
	jmp		.SetCarry1
.negativ:
	mov		ecx,-1		;ha negativ akkor az ecx=-1, csak a vegen tudom negalni
	jmp		.szamfelepit
.vege2:
	cmp		ecx,-1	;a vegen ha ecx=-1, akkor negalom
	jne		.theend
	neg		ebx
	jmp		.theend
.SetCarry1:
	mov		ecx,1
.theend:
	cmp		ecx,1		;ha ecx=1 akkor van hiba igy bele kell lepni az StC-be ahol beallitja a carryt, kulonben at kell ugorni azt es torolni a carryt
	jne		.SnC
.StC:
	stc
	jmp		.theendend	;ha mar beallitotta a carryt ki kell ugornia a carry torlese cimket
.SnC:
	clc
	
.theendend:
	mov		eax,ebx
	pop		esi
	pop		edi
	pop		ecx
	pop		edx
	pop		ebx
	ret
	
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
WriteInt:
		push ebx
		push ecx
		push eax
		push edx
	xor		edx,edx
	xor		ebx,ebx
	xor		ecx,ecx
	mov 	ebx, 10		;10-zel kell osztanom majd 
	cmp		eax, 0		;ha az eax nem kisebb mint nulla egybol ismetlesbe lepek
	jge		.loop
	neg		eax			;ha viszont negativ, akkor azt pozitivva kell alakitsam
	mov		ecx,eax
	mov		eax,'-'		;a pozitiv szam ele egy minusz jel
	call	mio_writechar
	mov		eax,ecx
	xor		ecx,ecx
	
.loop:
	xor		edx,edx			;elojel nelkul dolgozok
	div		ebx
	push	edx
	inc		ecx
	test	eax,eax		;ha nulla akkor vege lesz az ismetlesnek
	jnz		.loop
	
.loop2:
	pop 	eax
	add		eax,'0'		;mivel karakterkent kell hozza is kell adjam, hogy karakter legyen
	call	mio_writechar
	dec		ecx
	cmp		ecx, 0
	je		.vege
	jmp		.loop2
.vege:
	pop 	edx
	pop		eax
	pop		ecx
	pop		ebx
	ret
	
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ReadBin:
	push 	ebx
	push	ecx
	push	edx
	xor		eax,eax
	;mov		edi,szam
	mov		edx,edi		;megjegyzem az elejet, hogy ne torolhessen a backspace tobbet
	xor 	ecx,ecx		;szuksegem lesz ra jobb ha 0
	xor		ebx,ebx		;szuksegem lesz ra jobb ha 0
	
.ciklus:
	call	mio_readchar
	cmp		al,13
	je		.vege1
	cmp		al,8
	jne		.ugras			;ha nem backspace, nem kell kezelni, hanem kezelem
	cmp		edi,edx
	je		.ciklus
	call	mio_writechar
	mov		al,' '
	call	mio_writechar
	mov		al,8
	call	mio_writechar
	dec		edi
	cmp		edi,ebx			
	je		.ClCarry		;hogyha az edi az elso hiba helyen van (ebx) akkor torolhetem a hibat
	jmp		.ciklus
	
.ugras:
	call	mio_writechar
	stosb
	sub		al,'0'
	cmp		al,0
	jb		.SetCarry	;ha nem szamjegy be kell allitani a carryt (most ecx)
	cmp		al,1
	ja		.SetCarry
	jmp		.ciklus
.ClCarry:
	mov		ecx,0		;hibatorles
	jmp		.ciklus
.SetCarry:
	cmp		ecx,1		
	jne		.First		;ha meg nem volt hiba akkor megjegyzem az elso helyet es beallitom az ecx-et
	mov		ecx,1
	jmp		.ciklus
.First:
	mov		ebx,edi		;mivel meg nincs hiba, megjegyzem a helyet
	dec		ebx			;az edi mar tovabb lepett, igy csokkenteni kell
	mov		ecx,1
	jmp		.ciklus

.vege1:
	call	mio_writeln
	xor		al,al			;lezar
	stosb
	cmp		ecx,1			;ha van hiba akkor nem is kezdi a szamot felepiteni
	je		.theend
	;mov		esi,szam
	mov		esi,edx
	xor		ebx,ebx
	xor		edx,edx
.szamfelepit:
	inc		edx
	xor 	eax,eax
	lodsb
	cmp		al,0
	je		.theend		;ha vege a stringnek vege a szamnak is
	cmp		edx,32
	ja		.SetCarry1
	sub		al,'0'		;szamma 'alakitom' a karaktert
	shl		ebx,1
	add		ebx,eax
	jmp		.szamfelepit
.SetCarry1:
	mov		ecx,1

.theend:
	cmp		ecx,1		;ha ecx=1 akkor van hiba igy bele kell lepni az StC-be ahol beallitja a carryt, kulonben at kell ugorni azt es torolni a carryt
	jne		.SnC
.StC:
	stc
	jmp		.theendend	;ha mar beallitotta a carryt ki kell ugornia a carry torlese cimket
.SnC:
	clc
	
.theendend:
	mov		eax,ebx
	pop 	edx
	pop		ecx
	pop		ebx
	ret
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
WriteBin:
	push	eax
	push 	ebx
	push	ecx
	push	edx
	push	edi
	push	esi
	;xor		eax,eax
	xor		ebx,ebx
	xor		edx,edx
	xor		ecx,ecx
	xor		edi,edi
	;mov 	edi,0
	;mov 	eax,0x12345678
	mov 	ecx,32
	mov 	ebx,0x80000000
	
.kiirat:
	mov		edx,eax
	and		eax,ebx
	shr		eax,31
	add		eax,'0'
	call	mio_writechar
	shl		edx,1
	dec		ecx
	cmp		ecx,0
	je		.vege
	inc		edi
	cmp		edi,4
	je		.csoportosit
	mov		eax,edx
	cmp		ecx,0
	jne		.kiirat
	
.csoportosit:
	mov		eax,32
	call	mio_writechar
	mov		edi,0
	mov		eax,edx
	cmp		ecx,0
	jne		.kiirat
	
.vege:
	;call	mio_writeln
	pop		esi
	pop		edi
	pop 	edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ReadHex:	
	push 	ebx
	push	ecx
	push	edx
	xor		eax,eax
	;mov		edi,szam
	mov		edx,edi		;megjegyzem az elejet, hogy ne torolhessen a backspace tobbet
	xor 	ecx,ecx		;szuksegem lesz ra jobb ha 0
	xor		ebx,ebx		;szuksegem lesz ra jobb ha 0
	
.ciklus:
	call	mio_readchar
	cmp		al,13
	je		.vege1
	cmp		al,8
	jne		.ugras			;ha nem backspace, nem kell kezelni, hanem kezelem
	cmp		edi,edx
	je		.ciklus
	call	mio_writechar
	mov		al,' '
	call	mio_writechar
	mov		al,8
	call	mio_writechar
	dec		edi
	cmp		edi,ebx			
	je		.ClCarry		;hogyha az edi az elso hiba helyen van (ebx) akkor torolhetem a hibat
	jmp		.ciklus
	
.ugras:
	call	mio_writechar
	stosb
	cmp		al,48
	jb		.SetCarry	;ha nem szamjegy be kell allitani a carryt (most ecx)
	cmp		al,57
	ja		.ABC
	jmp		.ciklus
.ABC:
	cmp		al,65
	jb		.SetCarry	;ha nem nagybetu be kell allitani a carryt (most ecx)
	cmp		al,70
	ja		.abc
	jmp		.ciklus
.abc:
	cmp		al,97
	jb		.SetCarry	;ha nem kisbetu be kell allitani a carryt (most ecx)
	cmp		al,102
	ja		.SetCarry
	jmp		.ciklus
.ClCarry:
	mov		ecx,0		;hibatorles
	jmp		.ciklus
.SetCarry:
	cmp		ecx,1		
	jne		.First		;ha meg nem volt hiba akkor megjegyzem az elso helyet es beallitom az ecx-et
	mov		ecx,1
	jmp		.ciklus
.First:
	mov		ebx,edi		;mivel meg nincs hiba, megjegyzem a helyet
	dec		ebx			;az edi mar tovabb lepett, igy csokkenteni kell
	mov		ecx,1
	jmp		.ciklus

.vege1:
	call	mio_writeln
	xor		al,al			;lezar
	stosb
	cmp		ecx,1			;ha van hiba akkor nem is kezdi a szamot felepiteni
	je		.theend
	;mov		esi,szam
	mov		esi,edx
	xor		ebx,ebx
	xor		edx,edx
.szamfelepit:
	inc 	edx
	xor 	eax,eax
	lodsb
	cmp		al,0
	je		.theend		;ha vege a stringnek vege a szamnak is
	cmp		edx,8
	ja		.SetCarry1
	cmp		eax,97			;ha a-nal nagyobb kicsibetu
	jge		.kicsibetu
	cmp		eax,65			;ha A-nal nagyobb nagybetu
	jge		.nagybetu
	shl		ebx,4			;eltolasokkal epitem fel, csak szam maradhatott
	sub		eax,'0'
	add		bl,al
	jmp		.szamfelepit
.nagybetu:
	shl		ebx,4
	sub		eax,55		;mindenkepp szamma kell alakitsam
	add		ebx,eax
	jmp		.szamfelepit
.kicsibetu:
	shl		ebx,4
	sub		eax,87
	add		ebx,eax
	jmp		.szamfelepit
.SetCarry1:
	mov		ecx,1
.theend:
	cmp		ecx,1		;ha ecx=1 akkor van hiba igy bele kell lepni az StC-be ahol beallitja a carryt, kulonben at kell ugorni azt es torolni a carryt
	jne		.SnC
.StC:
	stc
	jmp		.theendend	;ha mar beallitotta a carryt ki kell ugornia a carry torlese cimket
.SnC:
	clc
	
.theendend:
	mov		eax,ebx
	pop 	edx
	pop		ecx
	pop		ebx
	ret
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
WriteHex:
	push	eax
	push	ebx
	push	edx
	push	ecx
	
	xor		ecx,ecx
	xor		edx,edx
	xor		ebx,ebx
	mov		ecx,8
	mov		ebx,0xF0000000		;ezzel hasonlitom hogy sorrendbe tudjam kiirni
	
.kezd:
	cmp		ecx,0			;mikor lejart a 8 karakter kilepik
	je		.vege
	mov		edx,eax
	and		eax,ebx			;itt a hasonlitas amivel csak a felso negy bit marad meg
	shr		eax,28			;ennyit kell toljam hogy ki tudjam iratni
	cmp		eax,9			;hogyha betu atugrok
	jg		.betuk
	add		eax,'0'
	call	mio_writechar
	dec		ecx
	mov		eax,edx
	shl		eax,4			;mindig a felso 4 bitet kapom meg igy elore kell tolni
	jmp		.kezd
	
.betuk:
	add		eax,55			;nagybetukkel iratom ki
	dec		ecx
	call	mio_writechar
	mov		eax,edx
	shl		eax,4			;ugyanugy kell tolni mint az elobb
	jmp		.kezd
	
.vege:
	pop	ecx
	pop	edx
	pop	ebx
	pop	eax
	ret
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ReadBin64:
	push 	ebx
	push	ecx
	push	edi
	push	esi
	xor		eax,eax
	;mov	edi,szam
	mov		esi,edi		;megjegyzem az elejet, hogy ne torolhessen a backspace tobbet
	xor 	ecx,ecx		;szuksegem lesz ra jobb ha 0
	xor		ebx,ebx		;szuksegem lesz ra jobb ha 0
	xor		edx,edx
.ciklus:
	call	mio_readchar
	cmp		al,13
	je		.vege1
	cmp		al,8
	jne		.ugras			;ha nem backspace, nem kell kezelni, hanem kezelem
	cmp		edi,esi
	je		.ciklus
	call	mio_writechar
	mov		al,' '
	call	mio_writechar
	mov		al,8
	call	mio_writechar
	dec		edi
	cmp		edi,ebx			
	je		.ClCarry		;hogyha az edi az elso hiba helyen van (ebx) akkor torolhetem a hibat
	jmp		.ciklus
	
.ugras:
	call	mio_writechar
	stosb
	sub		al,'0'
	cmp		al,0
	jb		.SetCarry	;ha nem szamjegy be kell allitani a carryt (most ecx)
	cmp		al,1
	ja		.SetCarry
	jmp		.ciklus
.ClCarry:
	mov		ecx,0		;hibatorles
	jmp		.ciklus
.SetCarry:
	cmp		ecx,1		
	jne		.First		;ha meg nem volt hiba akkor megjegyzem az elso helyet es beallitom az ecx-et
	mov		ecx,1
	jmp		.ciklus
.First:
	mov		ebx,edi		;mivel meg nincs hiba, megjegyzem a helyet
	dec		ebx			;az edi mar tovabb lepett, igy csokkenteni kell
	mov		ecx,1
	jmp		.ciklus

.vege1:
	call	mio_writeln
	xor		al,al			;lezar
	stosb
	cmp		ecx,1			;ha van hiba akkor nem is kezdi a szamot felepiteni
	je		.theend
	;mov		esi,szam
	;mov		esi,edx
	xor		ebx,ebx
	xor		edx,edx
	xor		edi,edi
.szamfelepit:
	inc		edi
	xor 	eax,eax
	lodsb
	cmp		al,0
	je		.theend		;ha vege a stringnek vege a szamnak is
	cmp		edi,64
	ja		.SetCarry1
	sub		al,'0'		;szamma 'alakitom' a karaktert
	shl		edx,1
	shl		ebx,1
	adc		edx,0
	add		ebx,eax
	jmp		.szamfelepit
.SetCarry1:
	mov		ecx,1

.theend:
	cmp		ecx,1		;ha ecx=1 akkor van hiba igy bele kell lepni az StC-be ahol beallitja a carryt, kulonben at kell ugorni azt es torolni a carryt
	jne		.SnC
.StC:
	stc
	jmp		.theendend	;ha mar beallitotta a carryt ki kell ugornia a carry torlese cimket
.SnC:
	clc
	
.theendend:
	mov		eax,ebx
	pop 	esi
	pop		edi
	pop		ecx
	pop		ebx
	ret
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
WriteBin64:
	push	eax
	push	edx
	push	ecx
	push	ebx
	push	edi
	push	esi
	mov		ebx,eax
	mov		eax,edx
	call	WriteBin
	mov		eax,' '
	call	mio_writechar
	mov		eax,ebx
	call	WriteBin
	pop		esi
	pop		edi
	pop		ebx
	pop		ecx
	pop		edx
	pop		eax
	ret
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ReadHex64:	
	push 	ebx
	push	ecx
	push	esi
	push	edi
	xor		edx,edx
	xor		eax,eax
	;mov	edi,szam
	mov		esi,edi		;megjegyzem az elejet, hogy ne torolhessen a backspace tobbet
	xor 	ecx,ecx		;szuksegem lesz ra jobb ha 0
	xor		ebx,ebx		;szuksegem lesz ra jobb ha 0
	
.ciklus:
	call	mio_readchar
	cmp		al,13
	je		.vege1
	cmp		al,8
	jne		.ugras			;ha nem backspace, nem kell kezelni, hanem kezelem
	cmp		edi,esi
	je		.ciklus
	call	mio_writechar
	mov		al,' '
	call	mio_writechar
	mov		al,8
	call	mio_writechar
	dec		edi
	cmp		edi,ebx			
	je		.ClCarry		;hogyha az edi az elso hiba helyen van (ebx) akkor torolhetem a hibat
	jmp		.ciklus
	
.ugras:
	call	mio_writechar
	stosb
	cmp		al,48
	jb		.SetCarry	;ha nem szamjegy be kell allitani a carryt (most ecx)
	cmp		al,57
	ja		.ABC
	jmp		.ciklus
.ABC:
	cmp		al,65
	jb		.SetCarry	;ha nem nagybetu be kell allitani a carryt (most ecx)
	cmp		al,70
	ja		.abc
	jmp		.ciklus
.abc:
	cmp		al,97
	jb		.SetCarry	;ha nem kisbetu be kell allitani a carryt (most ecx)
	cmp		al,102
	ja		.SetCarry
	jmp		.ciklus
.ClCarry:
	mov		ecx,0		;hibatorles
	jmp		.ciklus
.SetCarry:
	cmp		ecx,1		
	jne		.First		;ha meg nem volt hiba akkor megjegyzem az elso helyet es beallitom az ecx-et
	mov		ecx,1
	jmp		.ciklus
.First:
	mov		ebx,edi		;mivel meg nincs hiba, megjegyzem a helyet
	dec		ebx			;az edi mar tovabb lepett, igy csokkenteni kell
	mov		ecx,1
	jmp		.ciklus

.vege1:
	call	mio_writeln
	xor		al,al			;lezar
	stosb
	cmp		ecx,1			;ha van hiba akkor nem is kezdi a szamot felepiteni
	je		.theend
	;mov		esi,szam
	xor		ebx,ebx
	xor		edx,edx
	xor		edi,edi
.szamfelepit:
	inc 	edi
	xor 	eax,eax
	lodsb
	cmp		al,0
	je		.theend		;ha vege a stringnek vege a szamnak is
	cmp		edi,16
	ja		.SetCarry1
	cmp		eax,97			;ha a-nal nagyobb kicsibetu
	jge		.kicsibetu
	cmp		eax,65			;ha A-nal nagyobb nagybetu
	jge		.nagybetu
	push	edi
	shl		edx,4
	xor		edi,edi
	mov		edi,ebx
	shr		edi,28
	add		edx,edi
	shl		ebx,4			;eltolasokkal epitem fel, csak szam maradhatott
	sub		eax,'0'
	add		bl,al
	pop		edi
	jmp		.szamfelepit
.nagybetu:
	push	edi
	shl		edx,4
	xor		edi,edi
	mov		edi,ebx
	shr		edi,28
	add		edx,edi
	shl		ebx,4			;eltolasokkal epitem fel, csak szam maradhatott
	sub		eax,55
	add		bl,al
	pop		edi
	jmp		.szamfelepit
.kicsibetu:
	push	edi
	shl		edx,4
	xor		edi,edi
	mov		edi,ebx
	shr		edi,28
	add		edx,edi
	shl		ebx,4			;eltolasokkal epitem fel, csak szam maradhatott
	sub		eax,87
	add		bl,al
	pop		edi
	jmp		.szamfelepit
.SetCarry1:
	mov		ecx,1
.theend:
	cmp		ecx,1		;ha ecx=1 akkor van hiba igy bele kell lepni az StC-be ahol beallitja a carryt, kulonben at kell ugorni azt es torolni a carryt
	jne		.SnC
.StC:
	stc
	jmp		.theendend	;ha mar beallitotta a carryt ki kell ugornia a carry torlese cimket
.SnC:
	clc
	
.theendend:
	mov		eax,ebx
	pop 	edi
	pop		esi
	pop		ecx
	pop		ebx
	ret
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
WriteHex64:
	push	eax
	push	edx
	push	ebx
	push	ecx
	push	edi
	push	esi
	xchg	eax,edx
	call	WriteHex
	xchg	eax,edx
	call	WriteHex
	pop		esi
	pop		edi
	pop		ecx
	pop		ebx
	pop		edx
	pop		eax
	ret
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
WriteInt64:
	push	eax
	push	edx
	push	ebx
	push	ecx
	push	edi
	push	esi
	xor		ebx,ebx
	xor		ecx,ecx
	cmp		edx,0
	jge		.loop1
	push	eax
	mov		eax,'-'
	call	mio_writechar
	pop		eax
	not		eax
	not		edx
	add		eax,1
	adc		edx,0
.loop1:
	inc		ecx
	mov		ebx,10
	mov		edi,eax		;eax-et megjegyzem
	mov		eax,edx
	xor		edx,edx
	div		ebx			;maradek edx, hanyados eax
	mov		esi,eax		;hanyadost megjegyzem
	mov		eax,edi
	div		ebx			;maradek (ami kell ) edx-ben, hanyados eax-ben
	push	edx
	mov		edx,esi		;az egesz hanyados
	test	edx,edx
	jnz		.loop1
	test	eax,eax
	jnz		.loop1
	
.loop2:
	pop		eax
	add		eax,'0'
	call	mio_writechar
	dec		ecx
	cmp		ecx,0
	jne		.loop2
	
	pop		esi
	pop		edi
	pop		ecx
	pop		ebx
	pop		edx
	pop		eax
	ret
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ReadInt64:
	push	ebx
	push	ecx
	push	edi
	push	esi
	xor		eax,eax
	;mov	edi,szam
	mov		esi,edi		;megjegyzem az elejet, hogy ne torolhessen a backspace tobbet
	xor 	ecx,ecx		;szuksegem lesz ra jobb ha 0
	xor		ebx,ebx		;szuksegem lesz ra jobb ha 0
	
.ciklus:
	call	mio_readchar
	cmp		al,13
	je		.vege1
	cmp		al,8
	jne		.ugras			;ha nem backspace, nem kell kezelni, hanem kezelem
	cmp		edi,esi
	je		.ciklus
	call	mio_writechar
	mov		al,' '
	call	mio_writechar
	mov		al,8
	call	mio_writechar
	dec		edi
	cmp		edi,ebx			
	je		.ClCarry		;hogyha az edi az elso hiba helyen van (ebx) akkor torolhetem a hibat
	jmp		.ciklus
	
.ugras:
	call	mio_writechar
	stosb
	cmp		al,'-'		;ha - akkor lehet negativ
	je		.neg		;ha elso helyen van negativ lesz hanem hibanak szamit
	sub		al,'0'
	cmp		al,0
	jb		.SetCarry	;ha nem szamjegy be kell allitani a carryt (most ecx)
	cmp		al,9
	ja		.SetCarry
	jmp		.ciklus
.ClCarry:
	mov		ecx,0		;hibatorles
	jmp		.ciklus
.SetCarry:
	cmp		ecx,1		
	jne		.First		;ha meg nem volt hiba akkor megjegyzem az elso helyet es beallitom az ecx-et
	mov		ecx,1
	jmp		.ciklus
.First:
	mov		ebx,edi		;mivel meg nincs hiba, megjegyzem a helyet
	dec		ebx			;az edi mar tovabb lepett, igy csokkenteni kell
	mov		ecx,1
	jmp		.ciklus
.SetCarryandedx:
	mov		ecx,1
	dec		esi
	jmp		.ciklus
.neg:
	inc		esi					;muszaj novelnem mert az stosb utasitasra az edi mar nott egyet
	cmp		edi,esi
	jne		.SetCarryandedx		;ha nem az elso helyen van akkor hiba (ecx=1), de az edx-et is vissza kell allitani
	dec		esi
	;mov		ecx,-1				;ha az elso helyen van akkor megjegyzem hogy negativ szamom van
	jmp		.ciklus
	
.vege1:
	call	mio_writeln
	xor		al,al			;lezar
	stosb
	cmp		ecx,1			;ha van hiba akkor nem is kezdi a szamot felepiteni
	je		.theend
	;mov		esi,szam
	xor		ebx,ebx
	xor		edx,edx
	mov		edi,10
.szamfelepit:
	xor 	eax,eax
	lodsb
	cmp		al,0
	je		.vege2		;ha vege a stringnek vege a szamnak is
	cmp		al,'-'		;ha van akkor biztosan az elso helyen van	
	je		.negativ
	sub		al,'0'		;szamma 'alakitom' a karaktert
	push	eax
	push	ecx
	push	esi
	mov		ecx,edx
	mov		eax,ebx
	mul		edi
	mov		ebx,eax
	mov		esi,edx
	mov		eax,ecx
	mul		edi
	mov		edx,eax
	add		edx,esi
	pop		esi
	pop		ecx
	pop		eax
	add		ebx,eax
	adc		edx,0
	;cmp		ecx,0
	;je		.tulcsordulP
	;cmp		ecx,-1
	;je		.tulcsordulN
	jmp		.szamfelepit
;.tulcsordulP:
;	mov		edx,2147483647
;	imul	ebx,10
;	sub		edx,ebx
;	cmp		eax,edx
;	je		.JContinue
;	cmp		eax,edx
;	jg		.SetCarry1
;	add		ebx,eax
;	jmp		.szamfelepit
;.tulcsordulN:
;	mov		edx,2147483647
;	imul	ebx,10
;	sub		edx,ebx
;	sub		eax,1
;	cmp		eax,edx
;	je		.JContinueadd
;	cmp		eax,edx
;	jg		.SetCarry1
;	add		eax,1
;	add		ebx,eax
;	jmp		.szamfelepit
;.JContinueadd:
;	inc		eax
;.JContinue:
;	add		ebx,eax
;	xor 	eax,eax
;	lodsb
;	cmp		al,0
;	je		.vege2		;ha vege a stringnek vege a szamnak is, nem csordul tul
;	jmp		.SetCarry1
.negativ:
	mov		ecx,-1		;ha negativ akkor az ecx=-1, csak a vegen tudom negalni
	jmp		.szamfelepit
.vege2:
	cmp		ecx,-1	;a vegen ha ecx=-1, akkor negalom
	jne		.theend
	not		ebx
	not		edx
	add		ebx,1
	adc		edx,0
	jmp		.theend
.SetCarry1:
	mov		ecx,1
.theend:
	cmp		ecx,1		;ha ecx=1 akkor van hiba igy bele kell lepni az StC-be ahol beallitja a carryt, kulonben at kell ugorni azt es torolni a carryt
	jne		.SnC
.StC:
	stc
	jmp		.theendend	;ha mar beallitotta a carryt ki kell ugornia a carry torlese cimket
.SnC:
	clc
	
.theendend:
	mov		eax,ebx
	pop		esi
	pop		edi
	pop		ecx
	pop		ebx
	ret
