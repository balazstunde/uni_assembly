;beolvas egy előjeles 32 bites egész számot 10-es számrendszerben;
;kiírja a beolvasott értéket 10-es számrendszerben előjeles egészként, komplementer kódbeli ábrázolását 16-os és kettes számrendszerben;
;beolvas egy 32 bites hexa számot;
;kiírja a beolvasott értéket 10-es számrendszerben előjeles egészként, komplementer kódbeli ábrázolását 16-os és kettes számrendszerben;
;beolvas egy 32 bites bináris számot;
;kiírja a beolvasott értéket 10-es számrendszerben előjeles egészként, komplementer kódbeli ábrázolását 16-os és kettes számrendszerben;
;kiírja a három beolvasott érték összegét 10-es számrendszerben előjeles egészként, komplementer kódbeli ábrázolását 16-os és kettes számrendszerben;

%include 'IONUM.inc'
%include 'mio.inc'

global main
section .bss

szam1	resb 256
szam2	resb 256
szam3	resb 256
szam4	resb 256
szam5	resb 256
szam6 	resb 256
section .text
main:
	mov		eax,inmessage1
	call	mio_writestr
	call	mio_writeln
	mov		edi,szam1
	call 	ReadInt
	jnc		.kiir1
.beolvas1:
	mov		eax,hiba
	call	mio_writestr
	call	mio_writeln
	mov		eax,inmessage1
	call	mio_writestr
	call	ReadInt
	jc		.beolvas1
.kiir1:
	mov		edx,eax
	call	WriteInt
	call	mio_writeln
	call	WriteHex
	call	mio_writeln
	call	WriteBin
	call	mio_writeln
	
	mov		eax,inmessage2
	call	mio_writestr
	call	mio_writeln
	mov		edi,szam2
	call 	ReadHex
	jnc		.kiir2
.beolvas2:
	mov		eax,hiba
	call	mio_writestr
	call	mio_writeln
	mov		eax,inmessage2
	call	mio_writestr
	call	ReadHex
	jc		.beolvas2
.kiir2:
	mov		ebx,eax
	call	WriteInt
	call	mio_writeln
	call	WriteHex
	call	mio_writeln
	call	WriteBin
	call	mio_writeln
	
	mov		eax,inmessage3
	call	mio_writestr
	call	mio_writeln
	mov		edi,szam3
	call 	ReadBin
	jnc		.kiir3
.beolvas3:
	mov		eax,hiba
	call	mio_writestr
	call	mio_writeln
	mov		eax,inmessage3
	call	mio_writestr
	call	ReadBin
	jc		.beolvas3
.kiir3:
	call	WriteInt
	call	mio_writeln
	call	WriteHex
	call	mio_writeln
	call	WriteBin
	call	mio_writeln
	
	add		eax,edx
	add		eax,ebx
	call	WriteInt
	call	mio_writeln
	call	WriteHex
	call	mio_writeln
	call	WriteBin
	call	mio_writeln
	
;------------------------------------***------------------------------------
	mov		eax,inmessage4
	call	mio_writestr
	call	mio_writeln
	mov		edi,szam6
	call 	ReadInt64
	jnc		.kiir4
.beolvas4:
	mov		eax,hiba
	call	mio_writestr
	call	mio_writeln
	mov		eax,inmessage4
	call	mio_writestr
	call	mio_writeln
	mov		edi,szam4
	call 	ReadInt64
	jc		.beolvas4
.kiir4:
	mov		[a],edx			;[a]:[b]-ben az edx:eax
	mov		[b],eax
	call	WriteInt64
	call	mio_writeln
	call	WriteHex64
	call	mio_writeln
	call	WriteBin64
	call	mio_writeln
;------------------------------------***------------------------------------
	mov		eax,inmessage5
	call	mio_writestr
	call	mio_writeln
	mov		edi,szam6
	call 	ReadHex64
	jnc		.kiir5
.beolvas5:
	mov		eax,hiba
	call	mio_writestr
	call	mio_writeln
	mov		eax,inmessage5
	call	mio_writestr
	call	mio_writeln
	mov		edi,szam5
	call 	ReadHex64
	jc		.beolvas5
.kiir5:
	mov		[e],edx		;[e]:[f]-ben az edx:eax
	mov		[f],eax
	call	WriteInt64
	call	mio_writeln
	call	WriteHex64
	call	mio_writeln
	call	WriteBin64
	call	mio_writeln
;------------------------------------***------------------------------------
	mov		eax,inmessage6
	call	mio_writestr
	call	mio_writeln
	mov		edi,szam6
	call 	ReadBin64
	jnc		.kiir6
.beolvas6:
	mov		eax,hiba
	call	mio_writestr
	call	mio_writeln
	mov		eax,inmessage5
	call	mio_writestr
	call	mio_writeln
	mov		edi,szam6
	call 	ReadBin64
	jc		.beolvas6
.kiir6:
	call	WriteInt64
	call	mio_writeln
	call	WriteHex64
	call	mio_writeln
	call	WriteBin64
	call	mio_writeln
;------------------------------------***------------------------------------
	add		eax,[b]
	adc		edx,0
	add		eax,[f]
	adc		edx,0
	add		edx,[a]
	add		edx,[e]
	call	WriteInt64
	call	mio_writeln
	call	WriteHex64
	call	mio_writeln
	call	WriteBin64
	ret

section .data
	hiba		db	"Hiba!",0
	inmessage1	db  "Egy decimalis szamot: ",0
	inmessage2	db	"Egy hexadecimalis szamot: ",0
	inmessage3	db 	"Egy binaris szamot",0
	inmessage6	db 	"Egy binaris szamot(64)",0
	inmessage5	db 	"Egy hexadecimalis szamot(64)",0
	inmessage4	db 	"Egy decimalis szamot(64)",0
	outmessage1	db 	"Decimalis alak: ",0
	outmessage2	db	"Binaris alak: ",0
	outmessage3	db	"Hexadecimalis alak: ",0
	
	a dd 0
	b dd 0
	e dd 0
	f dd 0