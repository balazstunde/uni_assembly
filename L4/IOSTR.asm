;Balázs Éva-Tünde, beim1596, 511/1
;Labor4, C
;Készítsünk el egy olyan stringbeolvasó eljárást, amely megfelelőképpen kezeli le a backspace billentyűt (azaz visszalépteti a kurzort és letörli az előző karaktert). Teszteljük ezt az eljárást különböző kritikus esetekre (pl. a string elején a backspace-nek ne legyen hatása, valamint ha már több karaktert vitt be a felhasználó, mint a megengedett hossz és lenyomja a backspace-t akkor nem az elmentett utolsó karaktert kell törölni, hanem az elmentetlenekből az utolsót). <Enter>-ig olvas.

;Ebben a feladatban C stringekkel dolgozunk, itt a string végét a bináris 0 karakter jelenti.

;Készítsünk el egy olyan IOSTR.ASM / INC modult, amely a következő eljárásokat tartalmazza:

;ReadStr(EDI vagy ESI, ECX max. hossz):()   – C-s (bináris 0-ban végződő) stringbeolvasó eljárás, <Enter>-ig olvas
;WriteStr(ESI):()                                – stringkiíró eljárás
;ReadLnStr(EDI vagy ESI, ECX):()   – mint a ReadStr() csak újsorba is lép
;WriteLnStr(ESI):()                            – mint a WriteStr() csak újsorba is lép
;NewLine():()                                     – újsor elejére lépteti a kurzort
%include 'mio.inc'
global	ReadStr,ReadLnStr,WriteLnStr,WriteStr,NewLine

section .text
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ReadStr:
	push	eax
	push	ebx
	push	ecx
	push	edx
	mov		edx,edi		;megjegyzem az elejet, hogy ne torolhessen a backspace tobbet
	xor		ebx,ebx
	mov		ecx,256
	
.ciklus:
	call	mio_readchar
	cmp		al,13
	je		.vege
	cmp		al,8
	jne		.ugras			;ha nem backspace, nem kell kezelni
	cmp		edi,edx
	je		.ciklus
	call	mio_writechar
	mov		al,' '
	call	mio_writechar
	mov		al,8
	call	mio_writechar
	cmp		ebx,ecx
	ja		.JustDelete
	dec		ebx
	dec		edi
	jmp		.ciklus
	
.JustDelete:
	dec		ebx
	jmp		.ciklus
	
.ugras:
	inc		ebx
	cmp		ebx,ecx
	ja		.JustWrite
	call	mio_writechar
	stosb
	jmp		.ciklus
.JustWrite:
	call	mio_writechar
	jmp		.ciklus
	
.vege:
	call	mio_writeln
	xor		al,al			;lezar
	stosb
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
	
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ReadLnStr:
	push	eax
	push	ebx
	push	ecx
	push	edx
	call	ReadStr
	mov		al,13
	call	mio_writechar
	mov		al,10
	call	mio_writechar
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
WriteLnStr:
	push	eax
	push	ebx
	push	ecx
	push	edx
	call	WriteStr
	
	mov		al,13
	call	mio_writechar
	mov		al,10
	call	mio_writechar
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
	
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
WriteStr:
	push	eax
	push	ebx
	push	ecx
	push	edx
.loop:
	lodsb
	cmp		al,0		;mivel c, 0 zarokarakterig
	je		.end
	call	mio_writechar
	jmp		.loop
	
.end:
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
NewLine:
	push	eax
	push	ebx
	push	ecx
	push	edx
	mov     al, 13              ; carriage return
    call    mio_writechar
    mov     al, 10              ; line feed
    call    mio_writechar
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
	
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

