;Balázs Éva-Tünde, beim1596, 511/1
;Labor4, 3
;StrLen(ESI):(EAX)              – EAX-ben visszatéríiti az ESI által jelölt string hosszát, kivéve a bináris 0-t
;StrCat(EDI, ESI):()            – összefűzi az ESI és EDI által jelölt stringeket (azaz az ESI által jelöltet az EDI után másolja)
;StrUpper(ESI):()               – nagybetűssé konvertálja az ESI stringet
;StrLower(ESI):()               – kisbetűssé konvertálja az ESI stringet
;StrCompact(ESI):(EDI)      	– EDI-be másolja át az ESI stringet, kivéve a szóköz, tabulátor (9), kocsivissza (13) és soremelés (10) karaktereket                  – újsor elejére lépteti a kurzort
%include 'mio.inc'
global StrCat,StrCompact,StrLen,StrLower,StrUpper
section .bss

seged	resb 256
section .text
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
StrLen:
	push	ebx
	push	ecx
	push	edx
	xor		ebx,ebx
	xor		eax,eax
.loop:
	lodsb
	cmp		al,0		;mivel c, 0 zarokarakterig
	je		.end
	inc		ebx
	jmp		.loop
.end:
	mov		eax,ebx
	pop		edx
	pop		ecx
	pop		ebx
	ret

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
StrCat:
	push	eax
	push	ebx
	push	ecx
	push	edx
	push	edi
	push	esi
	xor		eax,eax
	
	push 	esi 
	mov		esi,edi
	call	StrLen
	pop 	esi
	add		edi,eax
	xor		eax,eax
	
.loop2:
	lodsb
	stosb
	cmp		al,0
	je		.end
	
	jmp		.loop2
	
.end:
	
	pop		esi
	pop		edi
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
StrUpper:
	push	eax
	push	ebx
	push	ecx
	push	edx
	push	esi
	push	edi
	xor		eax,eax
.loop1:
	lodsb
	cmp		al,0
	je		.end
	cmp		al,97
	jb		.loop1
	cmp		al,122
	ja		.loop1
	mov		edi,esi
	sub		edi,1
	sub		al,32
	stosb
	jmp		.loop1
.end:
	pop		edi
	pop		esi
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
	
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
StrLower:
	push	eax
	push	ebx
	push	ecx
	push	edx
	push	esi
	push	edi
	xor		eax,eax
.loop1:
	lodsb
	cmp		al,0
	je		.end
	cmp		al,65
	jb		.loop1
	cmp		al,90
	ja		.loop1
	mov		edi,esi
	sub		edi,1
	add		al,32
	stosb
	jmp		.loop1
.end:
	pop		edi
	pop		esi
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
	
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
StrCompact:
	push	eax
	push	ebx
	push	ecx
	push	edx
	mov		edi,seged
	mov		edx,edi
	xor		eax,eax
.loop1:
	lodsb
	cmp		al,0
	je		.end
	cmp		al,10
	je		.loop1
	cmp		al,13
	je		.loop1
	cmp		al,9
	je		.loop1
	cmp		al,32
	je		.loop1
	stosb
	jmp		.loop1
.end:
	xor		al,al
	stosb
	mov		edi,edx
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	