;Balázs Éva-Tünde, beim1596, 511/1
;Labor2, A
;Írjunk assembly eljárást (NASM), amely egy 32 bites, előjel nélküli egész számot ír ki a képernyőre bináris formában, 
;kötelezően mindig 32 bináris számjegy formájában (akkor is, ha 0-val kezdődik, pl: 0111 1011 0101 0100 1101 0010 1010 1011). 
;A kiírás során csoportosítsuk négyesével a biteket. Készítsünk el egy, a függvényeket alkalmazó rövid példaprogramot is, 
;amely az előző laborfeladatban szereplő hexadecimális pozitív egészet olvasó eljárást használva beolvas két számot, 
;majd kiírja a beolvasott két számot és az összegüket is binárisan.
;%include 'io.inc'
%include 'mio.inc'

global main 

section .text
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
binariskiir:
	push	eax
	push 	ebx
	push	ecx
	push	edx
	;xor		eax,eax
	xor		ebx,ebx
	xor		edx,edx
	xor		ecx,ecx
	xor		edi,edi
	;mov 	edi,0
	;mov 	eax,0x12345678
	mov 	ecx,32
	mov 	ebx,0x80000000
	
.kiirat:
	mov		edx,eax
	and		eax,ebx
	shr		eax,31
	add		eax,'0'
	call	mio_writechar
	shl		edx,1
	dec		ecx
	cmp		ecx,0
	je		.vege
	inc		edi
	cmp		edi,4
	je		.csoportosit
	mov		eax,edx
	cmp		ecx,0
	jne		.kiirat
	
.csoportosit:
	mov		eax,32
	call	mio_writechar
	mov		edi,0
	mov		eax,edx
	cmp		ecx,0
	jne		.kiirat
	
.vege:
	call	mio_writeln
	pop 	edx
	pop		ecx
	pop		ebx
	pop		eax
	ret

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

hexabeolvas:
	
	push 	ebx
	push	edx
	push	ecx
.eleje:
	xor		ebx,ebx
	xor		edx,edx
	xor		ecx,ecx
	xor		eax,eax
	mov		eax,hexa
	mov		ecx,8		;max 8 karaktert olvashatok be
	call	mio_writeln
	call	mio_writestr
	
.kezd:
	xor		eax,eax			;biztonsagos ha mindig nulla
	call	mio_readchar
	call	mio_writechar
	cmp		eax,13			;enterre kilepik
	je		.vege
	cmp		eax,97			;ha a-nal nagyobb kicsibetu
	jge		.kicsibetu
	cmp		eax,65			;ha A-nal nagyobb nagybetu
	jge		.nagybetu
	cmp		eax,'0'
	jl		.hiba
	cmp		eax,'9'			;hibakezeles
	jg		.hiba
	shl		ebx,4			;eltolasokkal epitem fel
	sub		eax,'0'
	add		bl,al
	dec		ecx
	;cmp	ecx,0
	;je		.vege
	jmp		.kezd
	
.nagybetu:
	cmp		eax,70		;hibakezeles
	jg		.hiba
	shl		ebx,4
	sub		eax,55		;mindenkepp szamma kell alakitsam
	add		ebx,eax
	dec		ecx
	;cmp		ecx,0
	;je		.vege
	jmp		.kezd
	
.kicsibetu:
	cmp		eax,102		;hibakezeles
	jg		.hiba
	shl		ebx,4
	sub		eax,87
	add		ebx,eax
	dec		ecx
	;cmp		ecx,0
	;je		.vege
	jmp		.kezd

.hiba:
	call	mio_writeln
	mov		eax,hiba
	call	mio_writestr
	jmp		.eleje		;visszaugrik az elejere
	
.vege:
	call	mio_writeln
	mov		eax,ebx
	pop 	ecx
	pop		edx
	pop		ebx
	ret
main:
	call	hexabeolvas
	;call	binariskiir
	mov		ebx,eax
	call	hexabeolvas
	xchg	eax,ebx
	call	binariskiir
	xchg	eax,ebx
	call	binariskiir
	mov		edx,eax
	mov		eax,osszeg
	call	mio_writestr
	call	mio_writeln
	mov		eax,edx
	add 	eax,ebx
	call	binariskiir
	
    ret
	
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


section .data
	hiba		db	"Hiba: A beolvasott szam nem decimalis alaku!",0
	hexa		db	"Egy hexa szamot kernek szepen:",0
	osszeg		db	"Az osszeg=",0