;Balázs Éva-Tünde, beim1596, 511/1
;Labor3, B
;Keszítsünk assembly programot (NASM), amely beolvas három előjel nélküli egész számot (használjuk a hexa olvasás függvényt) 32 bites egészként, 
;kiírja a felhasznált szabályt, mint üzenetet, és a beolvasott értékeket, mint 32 bites bináris egészeket, 
;külön soronként, majd előállítja és végül kiírja bináris formában a művelet eredményét, ami szintén egy 32 bites egész szám. 
;Az eredményt nem szabad több részletben kiiírni, az egyetlen érték kell legyen, amit elő kell állítani kiírás előtt! 
;A három értéket A, B, C sorrendben olvassuk be.

;7. C[8:7] AND 10, A[7:5] AND 010, 0, B[11:11] AND A[23:23], A[26:6], B[12:9] + A[31:28]
;%include 'io.inc'
%include 'mio.inc'

global main 

section .text
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
binariskiir:
	push	eax
	push 	ebx
	push	ecx
	push	edx
	;xor		eax,eax
	xor		ebx,ebx
	xor		edx,edx
	xor		ecx,ecx
	xor		edi,edi
	;mov 	edi,0
	;mov 	eax,0x12345678
	mov 	ecx,32
	mov 	ebx,0x80000000		;maszk
	
.kiirat:
	mov		edx,eax
	and		eax,ebx
	shr		eax,31			;bitenkent kiirat, cf-et is hasznalhattam volna
	add		eax,'0'
	call	mio_writechar
	shl		edx,1
	dec		ecx
	inc		edi
	cmp		edi,4
	je		.csoportosit
	mov		eax,edx
	cmp		ecx,0
	jne		.kiirat
	
.csoportosit:
	mov		eax,32
	call	mio_writechar
	mov		edi,0
	mov		eax,edx
	cmp		ecx,0
	jne		.kiirat
	
	call	mio_writeln
	pop 	edx
	pop		ecx
	pop		ebx
	pop		eax
	ret

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

hexabeolvas:
	
	push 	ebx
	push	edx
	push	ecx
.eleje:
	xor		ebx,ebx
	xor		edx,edx
	xor		ecx,ecx
	xor		eax,eax
	mov		eax,hexa
	mov		ecx,8		;max 8 karaktert olvashatok be
	call	mio_writeln
	call	mio_writestr
	
.kezd:
	xor		eax,eax			;biztonsagos ha mindig nulla
	call	mio_readchar
	call	mio_writechar
	cmp		eax,13			;enterre kilepik
	je		.vege
	cmp		eax,97			;ha a-nal nagyobb kicsibetu
	jge		.kicsibetu
	cmp		eax,65			;ha A-nal nagyobb nagybetu
	jge		.nagybetu
	cmp		eax,'0'
	jl		.hiba
	cmp		eax,'9'			;hibakezeles
	jg		.hiba
	shl		ebx,4			;eltolasokkal epitem fel
	sub		eax,'0'
	add		bl,al
	dec		ecx
	;cmp		ecx,0
	;je		.vege
	jmp		.kezd
	
.nagybetu:
	cmp		eax,70		;hibakezeles
	jg		.hiba
	shl		ebx,4
	sub		eax,55		;mindenkepp szamma kell alakitsam
	add		ebx,eax
	dec		ecx
	;cmp		ecx,0
	;je		.vege
	jmp		.kezd
	
.kicsibetu:
	cmp		eax,102		;hibakezeles
	jg		.hiba
	shl		ebx,4
	sub		eax,87
	add		ebx,eax
	dec		ecx
	;cmp		ecx,0
	;je		.vege
	jmp		.kezd

.hiba:
	call	mio_writeln
	mov		eax,hiba
	call	mio_writestr
	jmp		.eleje		;visszaugrik az elejere
	
.vege:
	call	mio_writeln
	mov		eax,ebx
	pop 	ecx
	pop		edx
	pop		ebx
	ret
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;7. C[8:7] AND 10, A[7:5] AND 010, 0, B[11:11] AND A[23:23], A[26:6], B[12:9] + A[31:28]
feldolgoz:
	push	edx		;A valtozo
	push	ecx		;C valtozo
	push	ebx		;B valtozo
	xor		eax,eax
	
	;C[8:7] AND 10		;elso 2 bit
	
	shr		ecx,7
	and		ecx,0x3
	and 	ecx,0x2
	shl		ecx,30
	add		eax,ecx
	;call	binariskiir
	
	;A[7:5] AND 010		;elso 5 bit
	
	mov		[a],edx		;meg kell hasznaljam igy nem ronthatom el veglegesen
	shr		edx,5
	and		edx,0x7
	and		edx,0x2
	shl		edx,27
	add		eax,edx
	;call	binariskiir
	
	;0 			;ezt most nem adom hozza, kovetkezobeneggyel kevesebbet tolok
	
	;B[11:11] AND A[23:23]	;elso 7 bit (itt egy plusz az elobb is egy)

	mov		edx,[a]
	mov		[b],ebx
	shr		ebx,11
	shr		edx,23
	and		ebx,0x1
	and		edx,0x1
	and		edx,ebx
	shl		edx,25
	add		eax,edx
	;call	binariskiir
	
	;A[26:6]		;elso 28 bit
	
	mov		edx,[a]
	shr		edx,6
	and		edx,0x1fffff
	shl		edx,4
	add		eax,edx
	;call	binariskiir
	
	;B[12:9] + A[31:28]		;mind a 32 bit
	
	mov		ebx,[b]
	mov		edx,[a]
	shr		ebx,9
	shr		edx,28
	add		ebx,edx
	and		ebx,0xf
	add		eax,ebx
	;call	binariskiir
		
	pop		ebx
	pop		ecx
	pop		edx
	ret

main:
	call	hexabeolvas
	mov		edx,eax			;A az edx-ben van
	call	hexabeolvas
	mov		ebx,eax			;B az ebx-ben van
	call	hexabeolvas
	mov		ecx,eax			;C az ecx-ben van
	mov 	eax,szabaly
	call	mio_writestr
	call	mio_writeln
	mov		eax,edx
	call	binariskiir
	mov		eax,ebx
	call	binariskiir
	mov		eax,ecx
	call	binariskiir
	call	feldolgoz
	call	binariskiir
    ret
	
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


section .data
	a dd 0
	b dd 0

	hiba		db	"Hiba: A beolvasott szam nem decimalis alaku!",0
	hexa		db	"Egy hexa szamot kernek szepen:",0
	osszeg		db	"Az osszeg=",0
	szabaly		db  " C[8:7] AND 10, A[7:5] AND 010, 0, B[11:11] AND A[23:23], A[26:6], B[12:9] + A[31:28]",0