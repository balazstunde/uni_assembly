;Balázs Éva-Tünde, beim1596, 511/1
;Labor3, C
;Készítsünk két assembly programot (NASM), amelyek beolvassák a szükséges karakterláncokat, kiírják a felhasznált szabályt (mint üzenetet) 
;és a beolvasott karakterláncokat külön sorokba, majd előállítják és végül kiírják a művelet eredményét, ami szintén egy karakterlánc.
;Az eredményt nem szabad több részletben kiiírni, az egyetlen karakterlánc kell legyen, amit elő kell állítani kiírás előtt! 
;A karakterláncok olvasásához és írásához írjunk alprogramot (a két feladatban különbözőek lesznek)!

;"abcde" + [A-ból azok a karakterek, amelyek "" jelek között vannak (minden második " jel után az a szakasz befejeződik és egy újabb " kell ahhoz, 
;hogy újabb szakasz nyíljon, valamint be is kell fejeződjön "-al, hogy érvényes legyen)] 
;+ "edcba" + [B, minden kisbetűt a rákövetkező betűvel helyettesítjük (kivéve a z-t, az marad)]
%include 'mio.inc'

global main 
section .bss

A	resb 256
B	resb 256
eredmeny resb 256

section .text
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;kiiras
kiiras:
.loop:
	lodsb
	cmp		al,0		;mivel c, 0 zarokarakterig
	je		.end
	call	mio_writechar
	jmp		.loop
	
.end:
	ret
	
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;beolvasas
beolvasas:
	mov		edx,edi		;megjegyzem az elejet, hogy ne torolhessen a backspace tobbet
	
.ciklus:
	call	mio_readchar
	cmp		al,13
	je		.vege
	cmp		al,8
	jne		.ugras			;ha nem backspace, nem kell kezelni
	cmp		edi,edx
	je		.ciklus
	call	mio_writechar
	mov		al,' '
	call	mio_writechar
	mov		al,8
	call	mio_writechar
	dec		edi
	jmp		.ciklus
	
.ugras:
	call	mio_writechar
	stosb
	jmp	.ciklus
	
.vege:
	call	mio_writeln
	xor		al,al			;lezar
	stosb
	ret

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
;feldolgoz
feldolgoz:
	push	eax
	push	ebx
	push	edx
	push	ecx
	push	edi
	push	esi
	xor		al,al
	
	;"abcde"
	mov 	edi,eredmeny
	mov		al,'a'
	stosb
	mov		al,'b'
	stosb
	mov		al,'c'
	stosb
	mov		al,'d'
	stosb
	mov		al,'e'
	stosb
	
	;[A-ból azok a karakterek, amelyek "" jelek között vannak (minden második " jel után az a szakasz befejeződik és egy újabb " kell ahhoz, hogy újabb szakasz nyíljon, valamint be is kell fejeződjön "-al, hogy érvényes legyen)] 
	mov		esi,A
.loop1:
	xor		ecx,ecx
	lodsb
	cmp		al,0
	je		.kovetkezo		;ha vege a karakterlancnak fuzhetem hozza a kovetkezo reszt
	cmp		al,34
	je		.loop2
	jmp		.loop1
	
.loop2:
	lodsb
	cmp		al,0
	je		.szintevege
	cmp		al,34
	je		.loop1
	inc		ecx
	stosb
	jmp		.loop2
	
.szintevege:
	sub		edi,ecx		;ha ide ugrott ki akkor nincs vege az idezojelnek, igy kivonom amit hozzaadta
	
	;"edcba"
.kovetkezo:
	mov		al,'e'
	stosb
	mov		al,'d'
	stosb
	mov		al,'c'
	stosb
	mov		al,'b'
	stosb
	mov		al,'a'
	stosb
	
	;[B, minden kisbetűt a rákövetkező betűvel helyettesítjük (kivéve a z-t, az marad)]
	mov		esi,B
.loop3:
	lodsb
	cmp		al,0
	je		.vege
	cmp		al,97		;ha kisebb mint a "a"
	jb		.nemkisbetu
	cmp		al,121		;ha nagyobb mint az "y"
	ja		.nemkisbetu
	inc		al
	stosb
	jmp		.loop3
	
.nemkisbetu:
	stosb
	jmp		.loop3
	
.vege:	
	xor		al,al		;lezar
	stosb
	pop		esi
	pop		edi
	pop		ecx
	pop		edx
	pop		ebx
	pop		eax
	ret
	
main:
	mov		eax,ki
	call	mio_writestr
	mov		edi,A
	call	beolvasas
	call	mio_writeln
	mov		eax,ki
	call	mio_writestr
	mov		edi,B
	call	beolvasas
	mov 	esi,A
	call	kiiras
	call	mio_writeln
	mov		esi,B
	call	kiiras
	call	mio_writeln
	mov		eax,feladatban
	call	mio_writestr
	call	mio_writeln
	call	feldolgoz
	mov		esi,eredmeny
	call	kiiras
    ret
	
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


section .data
	ki		dd 'Egy szoveget: ',0
	feladatban	dd '"abcde" + [A-ból azok a karakterek, amelyek "" jelek között vannak (minden második " jel után az a szakasz befejeződik és egy újabb " kell ahhoz, hogy újabb szakasz nyíljon, valamint be is kell fejeződjön "-al, hogy érvényes legyen)] + "edcba" + [B, minden kisbetűt a rákövetkező betűvel helyettesítjük (kivéve a z-t, az marad)]'
	