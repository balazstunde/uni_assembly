;Balázs Éva-Tünde, beim1596, 511/1
;Labor3, C
;Készítsünk két assembly programot (NASM), amelyek beolvassák a szükséges karakterláncokat, kiírják a felhasznált szabályt (mint üzenetet) 
;és a beolvasott karakterláncokat külön sorokba, majd előállítják és végül kiírják a művelet eredményét, ami szintén egy karakterlánc.
;Az eredményt nem szabad több részletben kiiírni, az egyetlen karakterlánc kell legyen, amit elő kell állítani kiírás előtt! 
;A karakterláncok olvasásához és írásához írjunk alprogramot (a két feladatban különbözőek lesznek)!

;"abcde" + [A-ból azok a karakterek, amelyek "" jelek között vannak (minden második " jel után az a szakasz befejeződik és egy újabb " kell ahhoz, 
;hogy újabb szakasz nyíljon, valamint be is kell fejeződjön "-al, hogy érvényes legyen)] 
;+ "edcba" + [B, minden kisbetűt a rákövetkező betűvel helyettesítjük (kivéve a z-t, az marad)]
%include 'mio.inc'

global main 
section .bss

A	resb 256
B	resb 256
eredmeny resb 256

section .text
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;kiiras
kiiras:
	push	eax
	push	ebx
	push	ecx
	push	edx
	push	edi
	xor		cl,cl
	xor 	al,al
	lodsb
	mov		cl,al		;elso biten hogy hany karakter van
.loop:
	cmp		cl,0		;ha nulla vege a kiirasnak
	je		.end
	lodsb
	call	mio_writechar
	dec		cl
	jmp		.loop
	
.end:
	pop		edi
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
	
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;beolvasas
beolvasas:
	push	eax
	push	ebx
	push	ecx
	push	edx
	push	edi
	
	mov		edx,edi
	inc		edx		;azert hogy a backspace ne tudjon visszabb lepni
	xor		ecx,ecx
	inc		edi		;elso hely szabad marad
	
.ciklus:
	call	mio_readchar
	cmp		al,13
	je		.vege
	cmp		al,8
	jne		.ugras
	cmp		edi,edx
	je		.ciklus
	dec		edi
	dec		cl			;ha backspace akkor a cl (amiben szamolom hogy hanz karakter van) is csokken
	call	mio_writechar
	mov		 al,' '
	call	mio_writechar
	mov		al,8
	call	mio_writechar
	jmp		.ciklus
	
.ugras:
	inc		cl		;ha betesz valamit akkor a cl no
	call	mio_writechar
	stosb
	jmp		.ciklus
	
.vege:
	call	mio_writeln
	mov		al,cl	;cl-ben hogy hany karaktere van
	dec		edx		;visszaallitom az elejere
	mov		edi,edx
	stosb
	
	pop		edi
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
;feldolgoz
feldolgoz:
	push	eax
	push	ebx
	push	edx
	push	ecx
	push	edi
	push	esi
	
	xor		bl,bl		;lenullaz
	xor		al,al
	xor		edx,edx
	
	;"abcde"
	mov 	edi,eredmeny
	mov		edx,edi				;edx-ben is az eredmeny kezdopontja
	inc		edi
	mov		al,'a'
	stosb
	mov		al,'b'
	stosb
	mov		al,'c'
	stosb
	mov		al,'d'
	stosb
	mov		al,'e'
	stosb
	add		dword[c],5		;c-ben szamolom hogy hany karaktere lesz az eredmenynek
	
	;[A-ból azok a karakterek, amelyek "" jelek között vannak (minden második " jel után az a szakasz befejeződik és egy újabb " kell ahhoz, hogy újabb szakasz nyíljon, valamint be is kell fejeződjön "-al, hogy érvényes legyen)] 
	mov		esi,A
	lodsb
	mov		bl,al		;bl-ben hogy hany karakteres a szoveg1
.loop1:
	cmp		bl,0
	je		.kovetkezo
	dec		bl
	xor		ecx,ecx
	lodsb
	cmp		al,34		;ha idezojel
	je		.loop2
	jmp		.loop1
	
.loop2:
	cmp		bl,0
	je		.szintevege
	lodsb
	dec		bl
	cmp		al,34
	je		.loop1
	inc		ecx
	stosb
	inc		dword[c]
	jmp		.loop2
	
.szintevege:
	sub		edi,ecx
	sub		[c],ecx
	
	;"edcba"
.kovetkezo:
	mov		al,'e'
	stosb
	mov		al,'d'
	stosb
	mov		al,'c'
	stosb
	mov		al,'b'
	stosb
	mov		al,'a'
	stosb
	add		dword[c],5
	
	;[B, minden kisbetűt a rákövetkező betűvel helyettesítjük (kivéve a z-t, az marad)]
	mov		esi,B
	lodsb
	mov		bl,al		;bl-ben hogy hany karakteres a szoveg2
.loop3:
	lodsb
	cmp		bl,0
	je		.vege
	inc		dword[c]
	dec		bl
	cmp		al,97
	jb		.nemkisbetu
	cmp		al,121
	ja		.nemkisbetu
	inc		al
	stosb
	jmp		.loop3
	
.nemkisbetu:
	stosb
	jmp		.loop3
	
.vege:	
	mov		edi,edx
	mov		al,[c]
	stosb
	pop		esi
	pop		edi
	pop		ecx
	pop		edx
	pop		ebx
	pop		eax
	ret
	
main:
	mov		eax,ki
	call	mio_writestr
	mov		edi,A
	call	beolvasas
	call	mio_writeln
	mov		eax,ki
	call	mio_writestr
	mov		edi,B
	call	beolvasas
	mov 	esi,A
	call	kiiras
	call	mio_writeln
	mov		esi,B
	call	kiiras
	call	mio_writeln
	mov		eax,feladatban
	call	mio_writestr
	call	mio_writeln
	call	feldolgoz
	mov		esi,eredmeny
	call	kiiras
    ret
	
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


section .data
	c dd 0
	
	ki		dd 'Egy szoveget:',0
	feladatban	dd '"abcde" + [A-bol azok a karakterek, amelyek "" jelek kozott vannak (minden masodik " jel utan az a szakasz befejezodik es egy ujabb " kell ahhoz, hogy ujabb szakasz nyiljon, valamint be is kell fejezodjon "-al, hogy ervenyes legyen)] + "edcba" + [B, minden kisbetut a rakovetkezo betuvel helyettesitjuk (kiveve a z-t, az marad)]'
	