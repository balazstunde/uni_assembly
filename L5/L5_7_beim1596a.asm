;Balázs Éva-Tünde, beim1596, 511/1
;Labor 5 SSE1
;Készítsünk el egy-egy olyan eljárást, amely 32 bites, egyszeres pontosságú lebegőpontos értéket olvas be és ír ki, 
;hagyományos (±1234.5678) és exponenciális formában (±1.234567e±12) egyaránt. 
;Az exponenciális formában a pozitív előjelek opcionálisak (tehát ez is helyes: 1.37e2), az e betű pedig lehet kicsi és nagy is.

;7. E(a,b,c,d) = (a^2 - b + c) / d + (a - b^3 + d) / c - 2.7
%include 'mio.inc'

global main 
section .text
;-------------------------------------------------------------------*******----------------------------------------------------------------------------------
ReadFloat:
	push	eax
	push	ebx
	push	ecx
	push	edx
	push	edi
	push	esi
	movss	xmm2,[k0_1]
	xorps		xmm0,xmm0
	xor			ecx,ecx
	xor			eax,eax
	call		mio_readchar
	call		mio_writechar
	cmp			al,13			;eloszor megnezem, hogy van-e pont vagy - jel, ha van akkor acx=-1, ezzel jelzem, hogy negativ
	je			.end
	cmp			al,'.'
	je			.loop2
	cmp			al,'-'
	jne			.Epit			;ha nem pont es nem is - jel, akkor a szamjegyet be kell epiteni a szamba
	mov			ecx,-1
	jmp			.loop1
.Epit:
	mulss		xmm0,[k10_0]
	sub			eax,'0'
	cvtsi2ss	xmm1,eax
	addss		xmm0,xmm1
.loop1:
	xor			eax,eax
	call		mio_readchar
	call		mio_writechar
	cmp			al,13
	je			.end
	cmp			al,'.'
	je			.loop2
	mulss		xmm0,[k10_0]			;mint az egesz szamoknal
	sub			eax,'0'
	cvtsi2ss	xmm1,eax
	addss		xmm0,xmm1
	jmp			.loop1
	
.loop2:
	xor			eax,eax
	call		mio_readchar
	call		mio_writechar
	cmp			al,13
	je			.end
	sub			eax,'0'
	cvtsi2ss	xmm1,eax			;ugyanugy csak 0.1-gyel szorzom majd 10^-2 stb
	mulss		xmm1,xmm2
	mulss		xmm2,[k0_1]
	addss		xmm0,xmm1
	jmp			.loop2
.end:
	cmp		ecx,-1
	jne		.theend
	movss	xmm1,[k0_0]
	movss	xmm2,xmm0
	subss	xmm1,xmm2		;negalas, kivonom 0-bol
	movss	xmm0,xmm1
.theend:
	pop		esi
	pop		edi
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
;-------------------------------------------------------------------*******----------------------------------------------------------------------------------
WriteFloat:
	push	eax
	push	ebx
	push	ecx
	push	edx
	push	edi
	push	esi
	movss		xmm7,xmm0		;hogy ne vesszen el, mert pusholni nem lehet
	movss		xmm2,[k0_0]
	movss		xmm1,xmm0
	comiss		xmm1,xmm2		;ha nagyobb 0-nal akkor semmi gond, kulonben negalom
	jae			.kezd
	mov		eax,'-'
	call	mio_writechar
	movss	xmm1,[k0_0]
	movss	xmm2,xmm0
	subss	xmm1,xmm2		;negalas
	movss	xmm0,xmm1
.kezd:
	xor		ecx,ecx
	xor		eax,eax
	CVTTSS2SI	eax,xmm0		;eax-ban lesz  az egesz resz
	cvtsi2ss	xmm2,eax
	movss		xmm1,xmm0
	subss		xmm1,xmm2		;kivonom az egeszreszt
	movss		xmm0,xmm1
	xor		ebx,ebx
	mov		ebx,10				;elokeszitem
	;call	io_writeint
.loop:
	xor		edx,edx			;elojel nelkul dolgozok
	div		ebx
	push	edx
	inc		ecx
	test	eax,eax		;ha nulla akkor vege lesz az ismetlesnek
	jnz		.loop
	
.loop2:
	pop 	eax
	add		eax,'0'		;mivel karakterkent kell hozza is kell adjam, hogy karakter legyen
	call	mio_writechar
	dec		ecx
	cmp		ecx, 0
	je		.SetEcx
	jmp		.loop2
	
.SetEcx:
	mov		ecx,4			;negy tizedes pontossag
	mov		eax,'.'
	call	mio_writechar
.loop3:
	mulss	xmm0,[k10_0]		;mindig szorzom 10-zel es az egeszreszt kiiratom
	cvttss2si	eax,xmm0
	add			eax,'0'
	call		mio_writechar
	sub			eax,'0'
	cvtsi2ss	xmm2,eax
	movss		xmm1,xmm0
	subss		xmm1,xmm2
	movss		xmm0,xmm1
	dec		ecx
	cmp		ecx,0
	jne		.loop3
.end:
	movss	xmm0,xmm7
	pop		esi
	pop		edi
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
;-------------------------------------------------------------------*******----------------------------------------------------------------------------------
ReadFloatExp:
	push	eax
	push	ebx
	push	ecx
	push	edx
	push	edi
	push	esi
	movss	xmm2,[k0_1]
	xorps	xmm0,xmm0
	xor			ecx,ecx
	xor			eax,eax
	xor			edx,edx					;az eleje akarcsak a ReadFloat-nal
	call		mio_readchar
	call		mio_writechar
	cmp			al,13
	je			.end
	cmp			al,'.'
	je			.loop2
	cmp			al,'-'
	jne			.Epit
	mov			ecx,-1
	jmp			.loop1
.Epit:
	mulss		xmm0,[k10_0]
	sub			eax,'0'
	cvtsi2ss	xmm1,eax
	addss		xmm0,xmm1
.loop1:
	xor			eax,eax
	call		mio_readchar
	call		mio_writechar
	cmp			al,'.'
	je			.loop2
	mulss		xmm0,[k10_0]
	sub			eax,'0'
	cvtsi2ss	xmm1,eax
	addss		xmm0,xmm1
	jmp			.loop1
	
.loop2:
	xor			eax,eax
	call		mio_readchar
	call		mio_writechar
	cmp			al,'e'			;ha e van a szamban, akkor megnezem, hogy utana negativ vagy pozitv hatvany kovetkezik
	je			.LeftOrRight
	cmp			al,'E'
	je			.LeftOrRight
	sub			eax,'0'
	cvtsi2ss	xmm1,eax
	mulss		xmm1,xmm2
	mulss		xmm2,[k0_1]
	addss		xmm0,xmm1
	jmp			.loop2
.LeftOrRight:
	movss		xmm1,xmm0
	call		mio_readchar
	call		mio_writechar
	cmp			al,'-'				;ha negativ akkor az xmm2-t maris 0.1-re allitom
	je			.SetXmmNeg
	cmp			al,'+'
	je			.SetXmmPoz			;ha pozitiv akkor xmm2=10
	sub			eax,'0'				;ha egyiksem akkor, pozitiv, ez is mar a hatvanykitevo
	imul		edx,10
	add			edx,eax
	jmp			.SetXmmPoz			;attol meg az xmm2-t be kell allitani
	
.SetXmmNeg:
	movss		xmm2,[k0_1]
	jmp			.felepit
.SetXmmPoz:
	movss		xmm2,[k10_0]
	jmp			.felepit
	
.felepit:
	call	mio_readchar			;felepiti a hatvanyt
	call	mio_writechar
	cmp		al,13
	je		.loop3
	sub		eax,'0'
	imul	edx,10
	add		edx,eax
	jmp		.felepit
	
.loop3:
	cmp		edx,0				;( arra fele ) annyit mozgatom a tizedes vesszot, amennyit mutat az egesz
	je		.end
	mulss	xmm1,xmm2
	dec		edx
	jmp		.loop3
.end:
	movss	xmm0,xmm1		;ha kell negalom
	cmp		ecx,-1
	jne		.theend
	movss	xmm1,[k0_0]
	movss	xmm2,xmm0
	subss	xmm1,xmm2		;negalas
	movss	xmm0,xmm1
.theend:
	pop		esi
	pop		edi
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
;-------------------------------------------------------------------*******----------------------------------------------------------------------------------
WriteFloatExp:
	push	eax
	push	ebx
	push	ecx
	push	edx
	push	edi
	push	esi
	xor		ecx,ecx
	xor		eax,eax
	xor		edx,edx
	xor		ebx,ebx
	movss		xmm2,[k0_0]
	movss		xmm1,xmm0
	comiss		xmm1,xmm2		;ha pozitiv akkor lehet vele dolgozni, kulonben negalni kell
	jae			.kezd
	movss		xmm2,xmm1
	movss		xmm1,[k0_0]
	subss		xmm1,xmm2		;negalas
	movss		xmm0,xmm1
	mov			eax,'-'
	call		mio_writechar
.kezd:
	CVTTSS2SI	eax,xmm0		;atalakitas csonkitassal, igy nezem meg, hogy hany szam van a tizedes elott
	movss		xmm1,xmm0
	;cvtsi2ss	xmm2,eax
	mov			ecx,-1
	cmp			eax,0			;ha a levagott szam 0, akkor negativ hatvanya lesz az exponencialis kitevonek, mivel szoroznom kell hogy elejrem az 1, alakot
	je			.Neg
	mov			ecx,1			;egyebkent pozitiv kitevo
	cmp			eax,9			;ha csak 1 van a tizedes elott akkor lehet kiirni, nem kell alakitani
	jbe			.CanWrite
	jmp			.Poz
	
.Neg:
	mulss		xmm1,[k10_0]	;szorzom 10-zel, amig nem nulla az eax
	inc			edx				;szamolom a hatvanykitevot
	cvttss2si	eax,xmm1
	cmp			eax,0
	jne			.CanWrite		
	jmp			.Neg
	
.Poz:
	mulss		xmm1,[k0_1]		;osztom 1/10-zel, amig az eax-ban csak egy szamjegy van
	inc			edx
	cvttss2si	eax,xmm1
	cmp			eax,9
	ja			.Poz
	jmp			.CanWrite
	
.CanWrite:
	cvttss2si	eax,xmm1		;a tizedes elotti szamjegyet kiiratom es ki is vonom
	add			eax,'0'
	call		mio_writechar
	mov			eax,'.'
	call		mio_writechar
	cvttss2si	eax,xmm1
	cvtsi2ss	xmm2,eax
	subss		xmm1,xmm2
	mov			ebx,6			;6 tizedes pontossag
	
.loop1:
	mulss		xmm1,[k10_0]	; mindig kiirja es kivonja az egeszreszt
	cvttss2si	eax,xmm1
	add			eax,'0'
	call		mio_writechar
	sub			eax,'0'
	cvtsi2ss	xmm2,eax
	subss		xmm1,xmm2
	dec			ebx
	cmp			ebx,0
	jne			.loop1
	
	mov		ebx,10				;kiir egy e betut
	mov		eax,'e'
	call	mio_writechar
	mov		eax,edx
	cmp		ecx,-1				;ha negativ kiir egy minuszt is
	jne		.SetEcx
	mov		eax,'-'
	call	mio_writechar
	mov		eax,edx
	;call	mio_writeln
	;call	io_writeint
.SetEcx:
	xor		ecx,ecx
	xor		edx,edx
.loop2:						;kiir egy egesz szamot, a hatvanyt
	div		ebx
	push	edx
	inc		ecx
	cmp		eax,0
	jne		.loop2
	
.loop3:
	pop		eax
	add		eax,'0'
	call	mio_writechar
	dec		ecx
	cmp		ecx,0
	jne		.loop3
	
	pop		esi
	pop		edi
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret
;-------------------------------------------------------------------*******----------------------------------------------------------------------------------
main:
	
	;a->xmm3
	
	mov			eax,szam1
	call		mio_writestr
	call		ReadFloat
	call		mio_writeln
	movss		xmm3,xmm0
	
	;b->xmm4
	mov			eax,szam2
	call		mio_writestr
	call		ReadFloatExp
	call		mio_writeln
	movss		xmm4,xmm0
	
	;c->xmm5
	mov			eax,szam1
	call		mio_writestr
	call		ReadFloat
	call		mio_writeln
	movss		xmm5,xmm0
	
	;d->xmm6
	mov			eax,szam1
	call		mio_writestr
	call		ReadFloatExp
	call		mio_writeln
	movss		xmm6,xmm0
	
	;E(a,b,c,d) = (a^2 - b + c) / d + (a - b^3 + d) / c - 2.7
	
	movss	xmm1,xmm3
	movss	xmm2,xmm3
	mulss	xmm1,xmm2		;xmm1 = a^2 (xmm3^2)
	
	movss	xmm2,xmm4
	subss	xmm1,xmm2		;xmm1 = a^2-b (xmm3^2 - xmm4)
	
	movss	xmm2,xmm5
	addss	xmm1,xmm2		;xmm1 = a^2-b+c (xmm3^2 - xmm4 + xmm5)
	
	movss	xmm2,xmm6
	divss	xmm1,xmm2		;xmm1 = (a^2-b+c) / d ((xmm3^2 - xmm4 + xmm5) / xmm6)
	
	movss	xmm7,xmm1		;xmm7 = (a^2-b+c) / d ((xmm3^2 - xmm4 + xmm5) / xmm6)
	
	movss	xmm1,xmm4
	movss	xmm2,xmm4
	mulss	xmm1,xmm2		;xmm1 = b^2
	mulss	xmm1,xmm2		;xmm1 = b^3
	
	movss	xmm2,xmm1
	movss	xmm1,xmm3
	subss	xmm1,xmm2		;xmm1 = a-b^3
	
	movss	xmm2,xmm6
	addss	xmm1,xmm2		;xmm1 = a-b^3+d
	
	movss	xmm2,xmm5
	divss	xmm1,xmm2		;xmm1 = (a-b^3+d)/c
	
	subss	xmm1,[k2_7]		;xmm1 = (a-b^3+d)/c - 2.7
	
	movss	xmm2,xmm1		;xmm2 = (a-b^3+d)/c - 2.7
	movss	xmm1,xmm7		;xmm1 = (a^2-b+c) / d
	addss	xmm1,xmm2		;xmm1 = (a^2-b+c) / d + (a-b^3+d)/c - 2.7
	
	movss	xmm0,xmm1		;xmm0 = xmm1
	
	mov 	eax,szam1
	call	mio_writestr
	call	WriteFloat
	call	mio_writeln
	
	mov		eax,szam2
	call	mio_writestr
	;movss	xmm0,[k12_234]
	call	WriteFloatExp
	
    ret
section .data
	k10_0 		dd 		10.0
	k12_234		dd		12.234
	k0_1 		dd 		0.1
	k0_0		dd		0.0
	k2_7		dd		2.7
	szam1		db		"Szam(hagyomanyosan) = ",0
	szam2		db		"Szam(exponencialisan) = ",0